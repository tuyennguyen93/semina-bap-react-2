import { all, takeLatest } from 'redux-saga/effects'

import sagaHelper from 'app/utils/saga-helper'
import { TYPES } from 'app/store/actions'

// Bước 4: lấy ra ở api/topics
import { createRegisterSchedules, updateRegisterSchedules, avgRate, 
  getAllByUserid, getAllByScheduleid, deleteRegisterSchedules } from 'app/api/register-schedules'

export default function* watcher() {
  yield all([
    takeLatest(
      TYPES.CREATE_REGISTER_SCHEDULES,
      sagaHelper({
        api: createRegisterSchedules
      })
    ),
    takeLatest(
      TYPES.UPDATE_REGISTER_SCHEDULES,
      sagaHelper({
        api: updateRegisterSchedules
      })
    ),
    takeLatest(
      TYPES.AVG_RATE,
      sagaHelper({
        api: avgRate
      })
    ),
    takeLatest(
      TYPES.AVG_RATE,
      sagaHelper({
        api: avgRate
      })
    ),
    takeLatest(
      TYPES.GET_ALL_BY_USERID,
      sagaHelper({
        api: getAllByUserid
      })
    ),
    takeLatest(
      TYPES.GET_ALL_BY_SCHEDULEID,
      sagaHelper({
        api: getAllByScheduleid
      })
    ),
    takeLatest(
      TYPES.DELETE_REGISTER_SCHEDULES,
      sagaHelper({
        api: deleteRegisterSchedules
      })
    )            
  ])
}
