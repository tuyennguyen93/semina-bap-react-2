import { all, takeLatest } from 'redux-saga/effects'

import sagaHelper from 'app/utils/saga-helper'
import { TYPES } from 'app/store/actions'
import Notification from 'app/utils/notification'
// Bước 4: lấy ra ở api/topics
import { getAllUsers, createUsers, updateUsers, deleteUsers, getProfile, updateProfile, changePassword, getUserById } from 'app/api/users'

export default function* watcher() {
  yield all([
    takeLatest(
      TYPES.GET_ALL_USERS,
      sagaHelper({
        api: getAllUsers
      })
    ),
    takeLatest(
      TYPES.CREATE_USERS,
      sagaHelper({
        api: createUsers
        // ,
        // successMessage: 'result',
        // errorHandler: async (errors) => {
        //   errors = await errors.json()
        //   Notification.error(errors.message)
        // }
      })
    ),
    takeLatest(
      TYPES.UPDATE_USERS,
      sagaHelper({
        api: updateUsers
        // ,
        // successMessage: 'result',
        // errorHandler: async (errors) => {
        //   errors = await errors.json()
        //   Notification.error(errors.message)
        // }
      })
    ),
    takeLatest(
      TYPES.DELETE_USERS,
      sagaHelper({
        api: deleteUsers
        // successMessage: 'result',
        // errorHandler: async (errors) => {
        //   errors = await errors.json()
        //   Notification.error(errors.message)
        // }
      })
    ),
    takeLatest(
      TYPES.GET_PROFILE,
      sagaHelper({
        api: getProfile
      })
    ),
    takeLatest(
      TYPES.UPDATE_PROFILE,
      sagaHelper({
        api: updateProfile
      })
    ),
    takeLatest(
      TYPES.CHANGE_PASSWORD,
      sagaHelper({
        api: changePassword
      })
    ),
    takeLatest(
      TYPES.GET_USER_BY_ID,
      sagaHelper({
        api: getUserById
      })
    )
  ])
}
