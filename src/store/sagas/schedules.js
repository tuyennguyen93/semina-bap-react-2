import { all, takeLatest } from 'redux-saga/effects'

import sagaHelper from 'app/utils/saga-helper'
import { TYPES } from 'app/store/actions'

// Bước 4: lấy ra ở api/topics
import { getAllSchedules, createSchedules, updateSchedules, deleteSchedules } from 'app/api/schedules'

export default function* watcher() {
  yield all([
    takeLatest(
      TYPES.GET_ALL_SCHEDULES,
      sagaHelper({
        api: getAllSchedules
      })
    ),
    takeLatest(
      TYPES.CREATE_SCHEDULES,
      sagaHelper({
        api: createSchedules
      })
    ),
    takeLatest(
      TYPES.UPDATE_SCHEDULES,
      sagaHelper({
        api: updateSchedules
      })
    ),
    takeLatest(
      TYPES.DELETE_SCHEDULES,
      sagaHelper({
        api: deleteSchedules
      })
    ),

  ])
}
