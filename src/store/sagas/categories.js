import { all, takeLatest } from 'redux-saga/effects'

import sagaHelper from 'app/utils/saga-helper'
import { TYPES } from 'app/store/actions'

// Bước 4: lấy ra ở api/topics
import { getAllCategories, createCategories, updateCategories, deleteCategories } from 'app/api/category'

export default function* watcher() {
  yield all([
    takeLatest(
      TYPES.GET_ALL_CATEGORIES,
      sagaHelper({
        api: getAllCategories
      })
    ),
    takeLatest(
      TYPES.CREATE_CATEGORIES,
      sagaHelper({
        api: createCategories
      })
    ),
    takeLatest(
      TYPES.UPDATE_CATEGORIES,
      sagaHelper({
        api: updateCategories
      })
    ),
    takeLatest(
      TYPES.DELETE_CATEGORIES,
      sagaHelper({
        api: deleteCategories
      })
    ),

  ])
}
