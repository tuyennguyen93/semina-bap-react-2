import { all, takeLatest } from "redux-saga/effects";

import sagaHelper from "app/utils/saga-helper";
import { TYPES } from 'app/store/actions'

// Bước 4: lấy ra ở api/topics
import { getProfile } from "app/api/user";

export default function* watcher() {
  yield all([
    takeLatest(
      TYPES.GET_PROFILE,
      sagaHelper({
        api: getProfile
      })
    )
  ])
}
