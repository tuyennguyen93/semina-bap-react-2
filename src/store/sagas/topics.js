import { all, takeLatest } from 'redux-saga/effects'

import sagaHelper from 'app/utils/saga-helper'
import { TYPES } from 'app/store/actions'

// Bước 4: lấy ra ở api/topics
import {
  getAllTopics,
  getTopicDetails,
  createTopics,
  updateTopics,
  deleteTopics,
  getAllApproveTopics,
  getAllDenyTopics,
  getAllMineTopics
} from 'app/api/topics'

export default function* watcher() {
  yield all([
    takeLatest(
      TYPES.GET_ALL_TOPICS,
      sagaHelper({
        api: getAllTopics
      })
    ),
    takeLatest(
      TYPES.GET_TOPIC_DETAILS,
      sagaHelper({
        api: getTopicDetails
      })
    ),
    // viết 1 saga đặt tên đúng với api/topics
    takeLatest(
      TYPES.CREATE_TOPICS,
      sagaHelper({
        api: createTopics
      })
    ),
    takeLatest(
      TYPES.GET_ALL_APPROVE_TOPICS,
      sagaHelper({
        api: getAllApproveTopics
      })
    ),
    takeLatest(
      TYPES.GET_ALL_DENY_TOPICS,
      sagaHelper({
        api: getAllDenyTopics
      })
    ),
    takeLatest(
      TYPES.GET_ALL_MINE_TOPICS,
      sagaHelper({
        api: getAllMineTopics
      })
    ),
    takeLatest(
      TYPES.UPDATE_TOPICS,
      sagaHelper({
        api: updateTopics
      })
    ),
    takeLatest(
      TYPES.DELETE_TOPICS,
      sagaHelper({
        api: deleteTopics
      })
    )
  ])
}
