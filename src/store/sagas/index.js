import { all } from 'redux-saga/effects'

import topics from './topics'
import users from './users'
import categories from './categories'
import schedules from './schedules'
import registerschedules from './register-schedules'

export default function* sagas() {
  yield all([
    topics(), categories(), users(), schedules(), registerschedules()
  ])
}
