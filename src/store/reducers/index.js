import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'

import topics from './topics'
import categories from './categories'
import users from './users'
import schedules from './schedules'
import registerSchedules from './register_schedules'

const appReducer = history => combineReducers({
  router: connectRouter(history),
  topics,
  categories,
  users,
  schedules,
  registerSchedules
})

export default history => (state, action) => {
  if (
    action.type === '@@router/LOCATION_CHANGE'
    && action.payload.location.pathname === '/login'
    && action.payload.action === 'PUSH'
  ) {
    state = undefined
  }

  return appReducer(history)(state, action)
}
