import { TYPES } from 'app/store/actions'
import lodash from 'lodash'

const INIT_STATE = {
  loading: false,
  categories: [],
  currentCate: null
}

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    //! STATUS REQUEST
    case TYPES.GET_ALL_CATEGORIES_REQUEST:
    case TYPES.CREATE_CATEGORIES_REQUEST:
    case TYPES.UPDATE_CATEGORIES_REQUEST:
    case TYPES.DELETE_CATEGORIES_REQUEST:
      return {
        ...state,
        loading: true
      }

      //! STATUS SUCCESS

    case TYPES.GET_ALL_CATEGORIES_SUCCESS:
      // console.log("this is status success")
      return {
        ...state,
        loading: false,
        categories: action.data
      }

    case TYPES.CREATE_CATEGORIES_SUCCESS:
      return {
        ...state,
        loading: false,
        categories: [action.payload].concat(state.categories)
      }

    case TYPES.UPDATE_CATEGORIES_SUCCESS: {
      const index = lodash.findIndex(state.categories, item => item.id === action.payload.id)
      return {
        ...state,
        loading: false,
        categories: state.categories.slice(0, index).concat([action.payload]).concat(state.categories.slice(index + 1, state.categories.length))
      }
    }
    case TYPES.DELETE_CATEGORIES_SUCCESS:
      console.log("hiiii")
      return {
        ...state,
        loading: false,
        categories: state.categories.filter(item => item.id !== action.payload)
      }


    //! STATUS FAILURE
    case TYPES.GET_ALL_CATEGORIES_FAILURE:
    case TYPES.CREATE_CATEGORIES_FAILURE:
    case TYPES.UPDATE_CATEGORIES_FAILURE:
    case TYPES.DELETE_CATEGORIES_FAILURE:

      //  console.log('this is status failure')
      return {
        ...state,
        loading: false
      }
    default:
      return state
  }
}
