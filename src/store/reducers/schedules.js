import { TYPES } from 'app/store/actions'
import lodash from 'lodash'

const INIT_STATE = {
  loading: false,
  schedules: [],
  currentCate: null
}

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    //! STATUS REQUEST
    case TYPES.GET_ALL_SCHEDULES_REQUEST:
    case TYPES.CREATE_SCHEDULES_REQUEST:
    case TYPES.UPDATE_SCHEDULES_REQUEST:
    case TYPES.DELETE_SCHEDULES_REQUEST:
      return {
        ...state,
        loading: true
      }

      //! STATUS SUCCESS

    case TYPES.GET_ALL_SCHEDULES_SUCCESS:
      // console.log("this is status success")
      return {
        ...state,
        loading: false,
        schedules: action.data
      }

    case TYPES.CREATE_SCHEDULES_SUCCESS:
      return {
        ...state,
        loading: false,
        schedules: [action.payload].concat(state.schedules)
      }

    case TYPES.UPDATE_SCHEDULES_SUCCESS: {
      const index = lodash.findIndex(state.schedules, item => item.id === action.payload.id)
      return {
        ...state,
        loading: false,
        schedules: state.schedules.slice(0, index).concat([action.payload]).concat(state.schedules.slice(index + 1, state.schedules.length))
      }
    }
    case TYPES.DELETE_SCHEDULES_SUCCESS:
      console.log("hiiii")
      return {
        ...state,
        loading: false,
        schedules: state.schedules.filter(item => item.id !== action.payload)
      }


    //! STATUS FAILURE
    case TYPES.GET_ALL_SCHEDULES_FAILURE:
    case TYPES.CREATE_SCHEDULES_FAILURE:
    case TYPES.UPDATE_SCHEDULES_FAILURE:
    case TYPES.DELETE_SCHEDULES_FAILURE:

      //  console.log('this is status failure')
      return {
        ...state,
        loading: false
      }
    default:
      return state
  }
}
