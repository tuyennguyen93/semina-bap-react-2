import { TYPES } from 'app/store/actions'
import lodash from 'lodash'

const INIT_STATE = {
  loading: false,
  currentUser: null,
  profile: null,
  users: [],
  submitting: null

}

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    //! STATUS REQUEST
    case TYPES.GET_PROFILE_REQUEST:
    case TYPES.UPDATE_PROFILE_REQUEST:
    case TYPES.CHANGE_PASSWORD_REQUEST:
    case TYPES.GET_ALL_USERS_REQUEST:
    case TYPES.CREATE_USERS_REQUEST:
    case TYPES.UPDATE_USERS_REQUEST:
    case TYPES.DELETE_USERS_REQUEST:
    case TYPES.GET_USER_BY_ID_REQUEST:
      return {
        ...state,
        loading: true,
        submitting: action.type
      }


    //! STATUS SUCCESS
    case TYPES.GET_PROFILE_SUCCESS:
      // console.log(action.data)
      return {
        ...state,
        loading: false,
        profile: action.data
      }
    case TYPES.UPDATE_PROFILE_SUCCESS:
      return {
        ...state,
        loading: false,
        profile: action.payload
      }
    case TYPES.GET_USER_BY_ID_SUCCESS:
      console.log(action.data)
      return {
        ...state,
        loading: false,
        profile: action.data
      }
    case TYPES.CHANGE_PASSWORD_SUCCESS:
      return {
        ...state,
        loading: false
      }

    case TYPES.GET_ALL_USERS_SUCCESS:
      console.log(action.data)
      return {
        ...state,
        loading: false,
        users: action.data,
        submitting: null
      }

    case TYPES.CREATE_USERS_SUCCESS:
      return {
        ...state,
        loading: false,
        //  users: [action.payload].concat(state.users),
        submitting: null
      }
    case TYPES.UPDATE_USERS_SUCCESS: {
      // const index = lodash.findIndex(state.users, item => item.id === action.payload.id)
      return {
        ...state,
        loading: false,
        // users: state.users.slice(0, index).concat([action.payload]).concat(state.users.slice(index + 1, state.users.length)),
        submitting: null
      } }
    case TYPES.DELETE_USERS_SUCCESS:
      return {
        ...state,
        loading: false,
        submitting: null
      }


    //! STATUS FAILURE
    case TYPES.GET_PROFILE_FAILURE:
    case TYPES.GET_ALL_USERS_FAILURE:
    case TYPES.CREATE_USERS_FAILURE:
    case TYPES.UPDATE_USERS_FAILURE:
    case TYPES.DELETE_USERS_FAILURE:
    case TYPES.GET_USER_BY_ID_FAILURE: 
      return {
        ...state,
        loading: false,
        submitting: null
      }


    default:
      return state
  }
}
