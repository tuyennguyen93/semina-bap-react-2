import { TYPES } from "app/store/actions"

const INIT_STATE = {
  loading: false,
  currentUser: null,
  profile: null
}

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    //! STATUS REQUEST
    case TYPES.GET_PROFILE_REQUEST:
      return {
        ...state,
        loading: true
      }

    //! STATUS SUCCESS
    case TYPES.GET_PROFILE_SUCCESS:
      console.log(`this is status success${state.profile}`);
      return {
        ...state,
        loading: false,
        profile: action.data
      }

    //! STATUS FAILURE
    case TYPES.GET_PROFILE_FAILURE:
      //  console.log('this is status failure')
      return {
        ...state,
        loading: false
      }

      
    default:
      return state
  }
}
