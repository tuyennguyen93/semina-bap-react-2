import { TYPES } from 'app/store/actions'
import lodash from 'lodash'

const INIT_STATE = {
  loading: false,
  topics: [],
  topicsApprove: [],
  topicsUnApprove: [],
  mineTopics: [],
  currentTopic: null
}

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    //! STATUS REQUEST
    case TYPES.GET_ALL_TOPICS_REQUEST:
    case TYPES.GET_TOPIC_DETAILS_REQUEST:
    case TYPES.CREATE_TOPICS_REQUEST:
    case TYPES.GET_ALL_APPROVE_TOPICS_REQUEST:
    case TYPES.GET_ALL_DENY_TOPICS_REQUEST:
    case TYPES.GET_ALL_MINE_TOPICS_REQUEST:
    case TYPES.UPDATE_TOPICS_REQUEST:
    case TYPES.DELETE_TOPICS_REQUEST:
    // console.log('this is status request')
      return {
        ...state,
        loading: true
      }

      //! STATUS SUCCESS

    case TYPES.GET_ALL_TOPICS_SUCCESS:
      console.log(action.data)
      return {
        ...state,
        loading: false,
        topics: action.data
      }

    case TYPES.GET_TOPIC_DETAILS_SUCCESS:
      return {
        ...state,
        loading: false,
        currentTopic: action.data
      }


    case TYPES.CREATE_TOPICS_SUCCESS:
      // console.log('gán tạo topic mới ở đây:', state.topics)
      return {
        ...state,
        loading: false,
        topics: [action.payload].concat(state.topics),
        topicsApprove: [action.payload].concat(state.topics),
        topicsUnApprove: [action.payload].concat(state.topics)

      }

    case TYPES.UPDATE_TOPICS_SUCCESS: {
      const index = lodash.findIndex(state.topics, item => item.id === action.payload.id)
      return {
        ...state,
        loading: false,
        topics: state.topics.slice(0, index).concat([action.payload]).concat(state.topics.slice(index + 1, state.topics.length)),
        topicsUnApprove: state.topicsUnApprove.slice(0, index).concat([action.payload]).concat(state.topicsUnApprove.slice(index + 1, state.topicsUnApprove.length)),
        topicsApprove: state.topicsApprove.slice(0, index).concat([action.payload]).concat(state.topicsApprove.slice(index + 1, state.topicsApprove.length))
      }
    }

    case TYPES.DELETE_TOPICS_SUCCESS:
      return {
        ...state,
        loading: false,
        topics: state.topics.filter(item => item.id !== action.payload),
        topicsApprove: state.topicsApprove.filter(item => item.id !== action.payload),
        topicsUnApprove: state.topicsUnApprove.filter(item => item.id !== action.payload)
      }

    case TYPES.GET_ALL_APPROVE_TOPICS_SUCCESS:
      console.log(action.data)
      return {
        ...state,
        loading: false,
        topicsApprove: action.data
      }

    case TYPES.GET_ALL_DENY_TOPICS_SUCCESS:
      return {
        ...state,
        loading: false,
        topicsUnApprove: action.data
      }

    case TYPES.GET_ALL_MINE_TOPICS_SUCCESS:
      return {
        ...state,
        loading: false,
        mineTopics: action.data
      }


    //! STATUS FAILURE
    case TYPES.GET_ALL_TOPICS_FAILURE:
    case TYPES.GET_TOPIC_DETAILS_FAILURE:
    case TYPES.CREATE_TOPICS_FAILURE:
    case TYPES.GET_ALL_APPROVE_TOPICS_FAILURE:
    case TYPES.GET_ALL_DENY_TOPICS_FAILURE:
    case TYPES.GET_ALL_MINE_TOPICS_FAILURE:
    case TYPES.UPDATE_TOPICS_FAILURE:
    case TYPES.DELETE_TOPICS_FAILURE:  
    //  console.log('this is status failure')
      return {
        ...state,
        loading: false
      }
    default:
      return state
  }
}
