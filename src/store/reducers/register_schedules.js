import { TYPES } from 'app/store/actions'
import lodash from 'lodash'

const INIT_STATE = {
  loading: false,
  registerSchedules: [],
  currentCate: null
}

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    //! STATUS REQUEST
    case TYPES.CREATE_REGISTER_SCHEDULES_REQUEST:
    case TYPES.UPDATE_REGISTER_SCHEDULES_REQUEST:
    case TYPES.AVG_RATE_REQUEST:
    case TYPES.GET_ALL_BY_USERID_REQUEST:
    case TYPES.GET_ALL_BY_SCHEDULEID_REQUEST:
    case TYPES.DELETE_REGISTER_SCHEDULES_REQUEST:
      return {
        ...state,
        loading: true
      }

      //! STATUS SUCCESS
    case TYPES.CREATE_REGISTER_SCHEDULES_SUCCESS:
      return {
        ...state,
        loading: false,
        registerSchedules: [action.payload].concat(state.registerSchedules)
      }

    case TYPES.UPDATE_REGISTER_SCHEDULES_SUCCESS:
      return {
        ...state,
        loading: false,
        registerSchedules: [action.payload].concat(state.registerSchedules)
      }

    case TYPES.AVG_RATE_SUCCESS:
      return {
        ...state,
        loading: false,
        registerSchedules: [action.payload].concat(state.registerSchedules)
      }

    case TYPES.GET_ALL_BY_USERID_SUCCESS:
      console.log(action.data)
      return {
        ...state,
        loading: false,
        registerSchedules: action.data
      }

    case TYPES.GET_ALL_BY_SCHEDULEID_SUCCESS:
      return {
        ...state,
        loading: false,
        registerSchedules: action.data
      }

    case TYPES.DELETE_REGISTER_SCHEDULES_SUCCESS:
      return {
        ...state,
        loading: false,
        registerSchedules: state.registerSchedules.filter(item => item.id !== action.payload)
      }

    //! STATUS FAILURE
    case TYPES.CREATE_REGISTER_SCHEDULES_FAILURE:
    case TYPES.UPDATE_REGISTER_SCHEDULES_FAILURE:
    case TYPES.AVG_RATE_FAILURE:
    case TYPES.GET_ALL_BY_USERID_FAILURE:
    case TYPES.GET_ALL_BY_SCHEDULEID_FAILURE:
    case TYPES.DELETE_REGISTER_SCHEDULES_FAILURE:

      return {
        ...state,
        loading: false
      }
    default:
      return state
  }
}
