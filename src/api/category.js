import { MainApi } from './endpoint'

export function getAllCategories() {
  return MainApi.get('/categories')
}

export function createCategories(data) {
  return MainApi.post('/categories', data)
}

export function updateCategories({ id, ...payload }) {
  return MainApi.put(`/categories/${id}`, payload)
}

export function deleteCategories(id) {
  return MainApi.delete(`/categories/${id}`)
}
