import { MainApi } from './endpoint'

export function getAllTopics() {
  return MainApi.get('/topics')
}

export function getTopicDetails(data) {
  return MainApi.get(`/topics/${data}`)
}

export function createTopics(data) {
  return MainApi.post('/topics', data)
}

export function updateTopics({ id, ...payload }) {
  return MainApi.put(`/topics/${id}`, payload)
}

export function deleteTopics(id) {
  return MainApi.delete(`/topics/${id}`,)
}

export function getAllApproveTopics() {
  return MainApi.get('/topics/status/1')
}


export function getAllDenyTopics() {
  return MainApi.get('/topics/status/0')
}

export function getAllMineTopics() {
  return MainApi.get('/topics/mine')
}
