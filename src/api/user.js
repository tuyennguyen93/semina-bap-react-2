import { MainApi } from './endpoint'

export function getProfile() {
  return MainApi.get('/api/v1/users/profile')
}

export function getAllUser(data) {
  return MainApi.get('/api/v1/users', data)
}
