import { MainApi } from './endpoint'

export function getProfile() {
  return MainApi.get('/profile')
}

export function updateProfile(data) {
  return MainApi.put('/profile', data)
}

export function changePassword(data) {
  return MainApi.put('/profile/changepassword', data)
}

export function getAllUsers(payload) {
  return MainApi.get('/users', payload)
}

export function getUserById(id) {
  return MainApi.get(`/users/${id}`)
}

export function createUsers(data) {
  return MainApi.post('/users', data)
}

export function updateUsers({ id, ...payload }) {
  return MainApi.put(`/users/${id}`, payload)
}

export function deleteUsers(id) {
  return MainApi.delete(`/users/${id}`)
}
