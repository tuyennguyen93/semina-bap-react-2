import { MainApi } from './endpoint'

export function getAllSchedules() {
  return MainApi.get('/schedules')
}

export function createSchedules(data) {
  return MainApi.post('/schedules', data)
}

export function updateSchedules({ id, ...payload }) {
  return MainApi.put(`/schedules/${id}`, payload)
}

export function deleteSchedules(id) {
  return MainApi.delete(`/schedules/${id}`)
}
