import { MainApi } from './endpoint'

export function getAllRegisterSchedules() {
  return MainApi.get('/registerSchedules')
}
// get by userid
export function getAllByUserid(id) {
  return MainApi.get(`/registerSchedules/listUser/${id}`)
}
// get by scheduleid
export function getAllByScheduleid() {
  return MainApi.get('/registerSchedules/listSchedule/')
}
// join
export function createRegisterSchedules(data) {
  return MainApi.post('/registerSchedules', data)
}
// rate
export function updateRegisterSchedules(data) {
  return MainApi.put('/registerSchedules', data)
}
// sum rate
export function avgRate(data) {
  return MainApi.post('/registerSchedules/avgrate', data)
}

export function deleteRegisterSchedules(id) {
  return MainApi.delete(`/registerSchedules/${id}`)
}
