import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'
import styled from 'styled-components'

import Storage from 'app/utils/storage'

const HeaderContainer = styled.header`
  background-color: gray;
  height: 60px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0 20px;

  a {
    color: white;
    font-weight: bold;
    margin-right: 10px;
  }

  button {
    background-color: transparent;
    border: 1px solid white;
    border-radius: 5px;
    color: white;
  }
`

class Header extends Component {
  constructor(props) {
    super(props)

    this._isAdmin = Storage.get('IS_ADMIN')
  }

  _onLogout = () => {
    const { history } = this.props

    Storage.remove('ACCESS_TOKEN')
    history.push('/login')
  }

  render() {
    const isAdmin = this._isAdmin

    return (
      <HeaderContainer>
        <div className="left">
          {isAdmin ? (
            <>
              <Link to="/schedules">Schedules</Link>
              <Link to="/topics">Topics</Link>
              <Link to="/admin/users">Users</Link>
            </>
          ) : (
            <>
              <Link to="/">Home</Link>
              <Link to="/schedules">Schedules</Link>
              <Link to="/topics">Topics</Link>
            </>
          )}
        </div>
        <div className="right">
          <button onClick={this._onLogout}>Logout</button>
        </div>
      </HeaderContainer>
    )
  }
}

export default withRouter(Header)
