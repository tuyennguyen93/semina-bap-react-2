import React, { Component, Suspense, lazy } from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'

import { Loading, Page } from 'app/components'
import Storage from 'app/utils/storage'
import Header from './header'
import LeftNav from './left-nav'


const Login = lazy(() => import('app/pages/auth/login'))
const NotFound = lazy(() => import('app/pages/not-found'))

/**
 * page home
 */
const Home = lazy(() => import('app/pages/home'))
/**
 * topics
 */
const Topics = lazy(() => import('app/pages/topics'))
const TopicsUser = lazy(() => import('app/pages/topics-user'))
/**
 * user
 */
const Users = lazy(() => import('app/pages/user'))
/**
 * profile
 */
const Profile = lazy(() => import('app/pages/profile'))

/**
 * categories
 */
const Categories = lazy(() => import('app/pages/categories'))

/**
 * schedules
 */
const Schedules = lazy(() => import('app/pages/schedules'))

/**
 * register-schedules
 */
const RegisterSchedules = lazy(() => import('app/pages/register-schedules'))

/**
 * history-join-schedules
 */
const HistoryJoinSchedules = lazy(() => import('app/pages/history-join-schedules'))

const AuthRoute = (props) => {
  const signedIn = Storage.has('ACCESS_TOKEN')
  return signedIn ? <Route {...props} /> : <Redirect to="/login" />
}
const PrivateRoute = ({ condition, redirect, ...props }) => {
  condition = condition()

  if (condition) return <Route {...props} />
  return <Redirect to={redirect} />
}

export default class extends Component {
  _renderLazyComponent = LazyComponent => props => <LazyComponent {...props} />

  // admin
  _renderAdminRouter = () => (
    <>
      {/* <Header /> */}
      <LeftNav />
      <Suspense fallback={<Page><Loading /></Page>}>
        <Switch>
          {/* <Route exact path="/admin" component={this._renderLazyComponent(Schedules)} /> */}
          <Route
            exact
            path="/admin/users"
            component={this._renderLazyComponent(Users)}
          />

          <Route
            exact
            path="/admin/topics"
            component={this._renderLazyComponent(Topics)}
          />

          <Route
            exact
            path="/admin/categories"
            component={this._renderLazyComponent(Categories)}
          />

          <Route
            exact
            path="/admin/schedules"
            component={this._renderLazyComponent(Schedules)}
          />

          <Route
            exact
            path="/admin/register-schedules"
            component={this._renderLazyComponent(RegisterSchedules)}
          />

           <Route
             exact
             path="/admin/history"
             component={this._renderLazyComponent(HistoryJoinSchedules)}
           />

          <Redirect to="/not-found" />
        </Switch>


      </Suspense>

    </>
  )

  // user login
  _renderAuthRoutes = () => (
    <>
      <LeftNav />
      <Suspense fallback={<Page><Loading /></Page>}>
        <Switch>
          <Route exact path="/" component={this._renderLazyComponent(Home)} />

          <Route
            exact
            path="/profile"
            component={this._renderLazyComponent(Profile)}
          />

          <Route
            exact
            path="/register-schedules"
            component={this._renderLazyComponent(RegisterSchedules)}
          />

          <Route
            exact
            path="/topics"
            component={this._renderLazyComponent(TopicsUser)}
          />

          <Route
            exact
            path="/history"
            component={this._renderLazyComponent(HistoryJoinSchedules)}
          />

          <Redirect to="/not-found" />
        </Switch>
      </Suspense>
    </>
  )

  // all
  render() {
    return (
      <Suspense fallback={<Page><Loading /></Page>}>
        <Switch>
          <Route path="/login" component={this._renderLazyComponent(Login)} />
          <Route path="/admin/login" component={this._renderLazyComponent(Login)} />
          <Route path="/not-found" component={this._renderLazyComponent(NotFound)} />
          <PrivateRoute
            condition={() => Storage.has('ACCESS_TOKEN') && Storage.get('IS_ADMIN')}
            redirect="/login"
            path="/admin"
            component={this._renderAdminRouter}
          />
          <PrivateRoute
            condition={() => Storage.has('ACCESS_TOKEN')}
            redirect="/login"
            path="/"
            component={this._renderAuthRoutes}
          />
        </Switch>
      </Suspense>
    )
  }
}
