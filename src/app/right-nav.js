import React, { Component } from 'react'
import { connect } from 'react-redux'
import { NavLink } from 'react-router-dom'
import { Drawer } from '@blueprintjs/core'
import styled from 'styled-components'

import { actions } from 'app/store/actions'

const StyledDrawer = styled(Drawer)`
  background-color: #ffd713;
`
const CloseButton = styled.span`
  cursor: pointer;
  font-size: 23px;
  position: absolute;
  right: 18px;
  top: 21px;

  &:hover {
    opacity: 0.8;
  }
`
const Content = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding-top: 62px;
`
const Logo = styled.span`
  font-size: 92px;
  margin-bottom: 25px;
`
const CompanyLogo = styled.span`
  position: absolute;
  left: 50%;
  transform: translateX(-50%);
  bottom: 30px;
  font-size: 22px;

  .path13::before {
    color: #2a2323;
  }
`
const NavButton = styled(NavLink)`
  font-size: 26px;
  color: #292929;
  letter-spacing: 0.7px;
  text-transform: uppercase;
  margin-bottom: 15px;
  transition: 0.2s;
  padding: 4px 12px;

  &.active {
    background-color: #292929;
    color: #ffd713;

    &:hover {
      color: #ffd713;
    }
  }

  &:hover {
    color: #292929;
    text-decoration: none;
    opacity: 0.7;
  }
`

@connect(state => ({
  uiStore: state.ui
}), {
  setRightNavStatus: actions.setRightNavStatus
})

export default class RightNav extends Component {
  _renderContent = () => {
    const { setRightNavStatus } = this.props

    return (
      <>
        <CloseButton
          className='icon-close'
          onClick={() => setRightNavStatus(false)}
        />
        <Content>
          <Logo className='icon-circle-logo' />
          <NavButton exact to='/' onClick={() => setRightNavStatus(false)}>main</NavButton>
        </Content>
        <CompanyLogo className='icon-company-logo'>
          <span className='path1' />
          <span className='path2' />
          <span className='path3' />
          <span className='path4' />
          <span className='path5' />
          <span className='path6' />
          <span className='path7' />
          <span className='path8' />
          <span className='path9' />
          <span className='path10' />
          <span className='path11' />
          <span className='path12' />
          <span className='path13' />
          <span className='path14' />
        </CompanyLogo>
      </>
    )
  }

  render () {
    const { uiStore, setRightNavStatus } = this.props

    return (
      <StyledDrawer
        onClose={() => setRightNavStatus(false)}
        isOpen={uiStore.isRightNavOpen}
        size='326px'
      >
        {this._renderContent()}
      </StyledDrawer>
    )
  }
}
