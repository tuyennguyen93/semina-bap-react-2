import React, { Component } from 'react'
import { Formik, Form, Field as FormikField } from 'formik'
import { connect } from 'react-redux'
import { Drawer, Position } from '@blueprintjs/core'
import { Link, NavLink, withRouter } from 'react-router-dom'
import * as Yup from 'yup'
import styled from 'styled-components'
import lodash from 'lodash'

import Storage from 'app/utils/storage'
import { actions, TYPES } from 'app/store/actions'
import { Button, Input } from 'app/components'
import { Layout, Menu, Breadcrumb, Icon } from 'antd'


const { Header, Content, Footer, Sider } = Layout
const { SubMenu } = Menu

// const StyleBar = styled.section`
//   .selectedLink{
//     background-color:red;
//   }
// `

export default class LeftNav extends Component {
  state = {
    collapsed: false
  }

  onCollapse = (collapsed) => {
    this.setState({ collapsed })
  }


  constructor(props) {
    super(props)
    this._isAdmin = Storage.get('IS_ADMIN')
  }

_onLogout = () => {
  const { history } = this.props
  // xóa đăng nhập , quay trở về login
  Storage.remove('ACCESS_TOKEN')
  history.push('/login')
}

render() {
  const isAdmin = this._isAdmin
  console.log(`kt ${isAdmin}`)

  return (
    <Layout style={{ minHeight: '100vh' }}>
        <Sider collapsible collapsed={this.state.collapsed} onCollapse={this.onCollapse}>
          <div className="logo" />
          <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">


            {isAdmin ? (
              <Menu.Item key="2">
               <NavLink to="/profile" activeClassName="selectedLink">
                <Icon type="gitlab" />
                <span> ADMIN</span>
               </NavLink>
              </Menu.Item>
            ) : (<></>)}

              {isAdmin ? (
              <Menu.Item key="1">
               <Link to="/">
                <Icon type="home" />
                <span> HOME</span>
               </Link>
              </Menu.Item>
              ) : (<></>)}

              {isAdmin ? (
              <Menu.Item key="3">
               <Link to="/admin/categories">
                <Icon type="folder-open" />
                <span> CATEGORY</span>
               </Link>
              </Menu.Item>
              ) : (<></>)}


              {isAdmin ? (
              <Menu.Item key="4">
               <Link to="/admin/topics">
                <Icon type="file" />
                <span> TOPICS</span>
               </Link>
              </Menu.Item>
              ) : (<></>)}

            {isAdmin ? (
              <Menu.Item key="5">
               <Link to="/admin/users">
                <Icon type="team" />
                <span> USERS</span>
               </Link>
              </Menu.Item>
            ) : (<></>)}
{/* 
              {isAdmin ? (
              <Menu.Item key="6">
               <Link to="/admin/schedules">
                <Icon type="schedule" />
                <span> SCHEDULE</span>
               </Link>
              </Menu.Item>
              ) : (<></>)}

              {isAdmin ? (
              <Menu.Item key="8">
               <Link to="/admin/register-schedules">
                <Icon type="form" />
                <span> JOIN NOW</span>
               </Link>
              </Menu.Item>
              ) : (<></>)}

              
              {isAdmin ? (
              <Menu.Item key="9">
               <Link to="/history">
               <Icon type="history" />
                <span>JOIN HISTORY</span>
               </Link>
              </Menu.Item>
              ) : (<></>)} */}
  
          {isAdmin ? (
            <SubMenu
              key="sub1"
              title={
                <span>
                  <Icon type="schedule" />
                  <span>SCHEDULE</span>
                </span>
              }
            >
               <Menu.Item key="6">
               <Link to="/admin/schedules">
                <Icon type="schedule" />
                <span> SCHEDULE</span>
               </Link>
              </Menu.Item>
           
              <Menu.Item key="8">
               <Link to="/admin/register-schedules">
                <Icon type="form" />
                <span> JOIN NOW</span>
               </Link>
              </Menu.Item>
             
              <Menu.Item key="9">
               <Link to="/admin/history">
               <Icon type="history" />
                <span>JOIN HISTORY</span>
               </Link>
              </Menu.Item>
            </SubMenu>
           ) : (<></>)} 
            {!isAdmin ? (
              <Menu.Item key="21">
               <Link to="/profile">
               <Icon type="user" />
                <span> USER</span>
               </Link>
              </Menu.Item>
            ) : (<></>)}

              {!isAdmin ? (
              <Menu.Item key="1">
               <Link to="/">
                <Icon type="home" />
                <span> HOME</span>
               </Link>
              </Menu.Item>
              ) : (<></>)}


              {!isAdmin ? (
              <Menu.Item key="24">
               <Link to="/topics">
                <Icon type="file" />
                <span> TOPICS</span>
               </Link>
              </Menu.Item>
              ) : (<></>)}

              {!isAdmin ? (
              <Menu.Item key="23">
               <Link to="/register-schedules">
               <Icon type="customer-service" />
                <span>SCHEDULE</span>
               </Link>
              </Menu.Item>
              ) : (<></>)}

              {!isAdmin ? (
              <Menu.Item key="25">
               <Link to="/history">
               <Icon type="history" />
                <span>JOIN HISTORY</span>
               </Link>
              </Menu.Item>
              ) : (<></>)}


            <Menu.Item key="34">
               <Link to="/logout" onClick={this._onLogout}>
                <Icon type="logout" />
                <span> LOGOUT</span>
               </Link>
            </Menu.Item>

          </Menu>
        </Sider>
        <Layout>
          <Header style={{ background: '#fff', padding: 0 }} />
          <Content style={{ margin: '0 16px' }}>
            {/* <Breadcrumb style={{ margin: '16px 0' }}>
              <Breadcrumb.Item>User</Breadcrumb.Item>
              <Breadcrumb.Item>Bill</Breadcrumb.Item>
            </Breadcrumb> */}
            {/* <div style={{ padding: 24, background: '#fff', minHeight: 360 }}>


            </div> */}
          </Content>
          {/* <Footer style={{ textAlign: 'center' }}> ©2019 Created by Ant TuyenNguyen</Footer> */}
        </Layout>
    </Layout>
  )
}
}
