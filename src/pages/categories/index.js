import React, { Component } from "react";
import { Table, Button, Popconfirm, Icon, message, Tag} from "antd";
import { connect } from "react-redux";
import "./style.scss";

import AddCategoriesModal from './add-categories-modal'
import EditCategoriesModal from './edit-categories-modal'


import { Container, FitHeightContainer, Page } from "app/components";
import { actions } from "app/store/actions";

@connect(state=>({ 
  categoriesStore:state.categories 
}),{
  getAllCategories:actions.getAllCategories, 
  createCategories:actions.createCategories,
  updateCategories:actions.updateCategories,
  deleteCategories:actions.deleteCategories

})

export default class TopicsWithRedux extends Component {

  /**
 * delete
 */

confirm = (e) => {
  const { deleteCategories } = this.props
  console.log(e.id)
  deleteCategories(e.id)
  message.success('Delete success!')
  
}

 cancel = (e) => {
  console.log(e)
  message.error('Delete failed!')
}

  componentDidMount() {
    this.props.getAllCategories(); 
  }

  render() {
    const { categories, loading } = this.props.categoriesStore; 
    const {createCategories,updateCategories} = this.props
    console.log(categories);
    

    const columns = [
      {
        title: 'No',
        dataIndex: 'no',
        key: 'no',
        render: (row, record, index) => index + 1
      },
      {
        title: "NAME",
        dataIndex: "name",
        key: "name"
      },
      {
        title: 'ACTION',
        key: 'action',
        render: (row, record) => (
           <span>
             <Button
               type="ghost"
               onClick={() => {
                 this._editCategoriesModal.open(record)
               }}
             ><Icon type="edit" /> 
             </Button>
 
             <Popconfirm
               title="Are you sure delete this Category?"
               onConfirm={() => this.confirm(record)}
               onCancel={this.cancel}
               okText="Yes"
               cancelText="No"
             >
               <a href="#"><Button type="danger"><Icon type="delete" /></Button></a>
            </Popconfirm>

           </span>
        )
      }
    ];

    return (
      <Page className="home">
        <FitHeightContainer hasHeader>
          <Container>
            <h2> CATEGORY MANAGEMENT</h2>
            <br/>
            <Button
              type="primary"
              onClick={() => {
                this._addCategoriesModal.open()
              }}
            ><Icon type="folder-open" />
              Add Category
            </Button> 

            <br/>  <br/>

            <Table
              rowKey={(row, index) => index} 
              pagination={true}
              loading={loading}
              dataSource={categories}
              columns={columns}
             
            />

            <AddCategoriesModal           
              createCategories={createCategories}
              ref={(ref) => { this._addCategoriesModal = ref }}
            /> 

            <EditCategoriesModal
              updateCategories={updateCategories}
              ref={(ref) => { this._editCategoriesModal = ref }}
            />
          </Container>
        </FitHeightContainer>
      </Page>
    );
  }
}


