import React from 'react'
import { Modal, Button, Input, Row, Col, Select, Tag, message } from 'antd'
import { Formik, Field, Form } from 'formik'
import * as yup from 'yup'
import { TYPES } from 'app/store/actions'
import './style.scss'

const schemaValidation = yup.object().shape({
  name: yup.string()
})

class addUserModal extends React.Component {
  state = {
    visible: false,
    user: null
  }

  open = (user) => {
    this.setState({
      visible: true,
      user
    })
  };

  close = (e) => {
    this.setState({
      visible: false,
      user: null
    })
  }

  _onSelectChange = (value) => {
    console.log(value)
  }

  _submit = () => {
    this.close()
    console.log('aaa')
  }

  // event
  _onSubmit = (value) => {
    const { updateCategories } = this.props
    updateCategories(value, (action) => {
      if (action === TYPES.UPDATE_CATEGORIES_SUCCESS) {
        this.close()
        message.success('Successfully update category!')
      } else {
        this.close()
        message.error('err updated !')
      }
    })


    // this.close()
    // console.log(value)
  }

  render() {
    const { user } = this.state
    console.log(user)
    // fix trung data o modal
    const initialValues = user || {
      name: ''
    }
    return (
      <div>
          <Modal
            title="Edit Topics"
            visible={this.state.visible}
            onCancel={this.close}
            onOk={this._onSubmit}
            footer={null} // ko muốn hiện footer
          >
              <Formik
                enableReinitialize
                initialValues={initialValues}
                validationSchema={schemaValidation}
                onSubmit={this._onSubmit}

                render={({ handleSubmit }) => (
                      <Form>
                      {/* name */}
                      <div className="field">
                              <p className="label">Name</p>
                              <Field
                                name="name"
                                render={({ field, form }) => (
                                  <div>
                                  <Input {...field} />
                                  <p className="error-message">{form.touched.name && form.errors.name}</p>
                                  {/* fix hiện Lỗi sau khi nhập dữ liệu */}
                                  </div>
                                )}
                              />
                      </div>
                      <Button type="primary" onClick={handleSubmit}>Update</Button>
                      <Button type="default" onClick={() => this.close()}id="btnCancelModal">cancel</Button>
                      </Form>
                )}
              />
          </Modal>
      </div>
    )
  }
}

export default addUserModal
