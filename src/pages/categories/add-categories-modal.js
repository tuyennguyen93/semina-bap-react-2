import React from 'react'
import { Modal, Button, Input, message } from 'antd'
import { Formik, Field, Form } from 'formik'
import * as yup from 'yup'
import { TYPES } from 'app/store/actions'
import './style.scss'


const schemaValidation = yup.object().shape({
  name: yup.string().required()
})

class addUserModal extends React.Component {
  state = {
    visible: false,
    user: null
  }

  open = (user) => {
    this.setState({
      visible: true,
      user
    })
  };

  close = (e) => {
    this.setState({
      visible: false,
      user: null
    })
  }

  // event
  _onSubmit = (value) => {
    console.log(value)
    const { createCategories } = this.props
    createCategories(value, (action) => {
      if (action === TYPES.CREATE_CATEGORIES_SUCCESS) {
        this.close()
        message.success('create category success')
      } else {
        this.close()
        message.error('Category name is already in use ')
      }
    })
  }

  render() {
    const { user } = this.state
    const initialValues = user || {
      name: ''
    }
    return (
      <div>
          <Modal
            title="Add new Category "
            visible={this.state.visible}
            onCancel={this.close}
            onOk={this._onSubmit}
            footer={null} 
          >
              <Formik
                enableReinitialize
                initialValues={initialValues}
                validationSchema={schemaValidation}
                onSubmit={this._onSubmit}

                render={({ handleSubmit }) => (
                      <Form>
                        {/* categories */}
                        <div className="field">
                                <p className="label">Category Name</p>
                                <Field
                                  name="name"
                                  render={({ field, form }) => (
                                    <div>
                                    <Input {...field} />
                                    <p className="error-message">{form.touched.name && form.errors.name}</p>
                                    </div>
                                  )}
                                />
                        </div>

                        <Button type="primary" onClick={handleSubmit}>Add</Button>
                        <Button type="default" onClick={() => this.close()} id="btnCancelModal">Cancel</Button>
                      </Form>
                )}
              />
          </Modal>
      </div>
    )
  }
}

export default addUserModal
