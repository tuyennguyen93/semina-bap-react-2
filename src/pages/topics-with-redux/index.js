import React, { Component } from "react";
import { Table } from "antd";
import { connect } from "react-redux";
import "./style.scss";

import AddTopicForm from "./add-topic-form";
import { Container, FitHeightContainer, Page } from "app/components";
import { actions } from "app/store/actions";
@connect(state=>({ //lấy dữ liệu  state từ reducers 
  topicsStore:state.topics // lấy cái này : topics: [], //lưu giá trị vào đây
}),{
  getAllTopics:actions.getAllTopics //gọi action từ saga 
})

export default class TopicsWithRedux extends Component {
  componentDidMount() {
    this.props.getAllTopics(); //gọi từ index trong action GET_ALL_TOPICS =>getAllTopics
    
  }

  render() {
    const { topics, loading } = this.props.topicsStore; //*

    const columns = [
      {
        title: "Name2",
        dataIndex: "name",
        key: "name"
      },
      {
        title: "Description",
        dataIndex: "description",
        key: "description"
      },
      {
        title: "Author",
        dataIndex: "author",
        key: "author"
      }
    ];

    return (
      <Page className="home">
        <FitHeightContainer hasHeader>
          <Container>
            <h2>Topic List (dùng Redux)</h2>
            <AddTopicForm />
            <Table
              rowKey={(row, index) => index} //fix lỗi key  bằng cách tạo ra các index ++ ở các dòng
              pagination={false}
              loading={loading}
              dataSource={topics}
              columns={columns}
              onRow={row => ({
                onClick: () => {
                  this.props.history.push("/topics-detail/" + row.key);
                  console.log(row);
                }
              })}
            />
          </Container>
        </FitHeightContainer>
      </Page>
    );
  }
}

// const mapStateToProps = state => {
//   return {
//     topicsStore: state.topics
//   };
// };

// const mapDispatchToProps = {
//   getAllTopics: actions.getAllTopics
// };

// export default connect(
//   mapStateToProps,
//   mapDispatchToProps
// )(TopicsWithRedux); //*
