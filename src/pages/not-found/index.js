import React, { Component } from 'react'
import './style.scss'
import './style.css'

import { Container } from 'app/components'

export default class NotFound extends Component {
  _onClick = () => {
    const { history } = this.props

    history.push('/')
  }

  render() {
    return (
      <Container className="not-found">
        {/* <div className="content">
          <p className="title">Lỗi 404</p>
          <p className="subtitle">This is not the web page you are looking for</p>
        </div> */}
        <body >
        <div className="error__wrapper" id="body_error">
          <div className="orbits orbit_1">
            <div className="planets planet_1" />
            <div className="planets planet_2" />
            <div className="planets planet_3" />
          </div>
          <div className="orbits orbit_2">
            <div className="planets planet_1" />
            <div className="planets planet_2" />
            <div className="planets planet_3" />
          </div>
          <div className="orbits orbit_3">
            <div className="planets planet_1" />
            <div className="planets planet_2" />
            <div className="planets planet_3" />
          </div>
          <div className="orbits orbit_4">
            <div className="planets planet_1" />
            <div className="planets planet_2" />
          </div>
          <div className="error__contant">
            <h3 className="error__title">404</h3>
            <h4 className="error__subtitle">Sorry, the page you were looking for doesn't exist.</h4>
            <a
              href="/"
              className="error__link-back"
            > Go back
            </a>
          </div>
        </div>
        </body>
      </Container>
    )
  }
}
