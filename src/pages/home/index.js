import React, { Component } from "react";
import { Table, Tabs, Icon, Tag } from "antd";
import { connect } from "react-redux";
import "./style.css";
import "./style.scss";
import { Container, FitHeightContainer, Page } from "app/components";
import { actions } from "app/store/actions";
const { TabPane } = Tabs;

@connect(state=>({ //lấy dữ liệu  state từ reducers 
  topicsStore:state.topics // lấy cái này : topics: [], //lưu giá trị vào đây
}),{
  getAllTopics:actions.getAllTopics //gọi action từ saga 
})

export default class TopicsWithRedux extends Component {
  componentDidMount() {
    this.props.getAllTopics(); //gọi từ index trong action GET_ALL_TOPICS =>getAllTopics
    
  }

  render() {
    const { topics, loading } = this.props.topicsStore; //*

   

    return (
      <Page className="home">
        <FitHeightContainer hasHeader>
          <Container>
             <div id="home"> 
           <img src="https://bap.jp/wp-content/uploads/2019/06/commitments-3.webp"></img>    
            
           <p>
           <Tag color="red">Admin</Tag><Icon type="notification" />Xin chào các bạn! <br/>
          Trong số các bạn,<br/>
          - Có ai đang cảm thấy công việc cần thêm một chút nhộn nhịp?<br/>
          - Có ai đang cảm thấy đôi tay phải hoạt động nhiều nhưng đôi tai lại quá rảnh rang?<br/>
          - Và có ai muốn cho đôi tai được nghe những lời mật ngọt êm dịu đến từ SẾP?<br/>
           Vậy thì SEMINAR THÁNG 7 cùng với speaker <Tag color="gold">Nguyễn Tuấn Sơn</Tag> chính là cơ hội hấp dẫn như lời tỏ tình của Hot girl!<br/>
           Mọi người cũng đừng quên ĐÁNH GIÁ BUỔI SEMINAR vừa qua nha.
           Đây sẽ là một hoạt động thường xuyên, và đánh giá của mọi người rất quan trọng cho những buổi seminar sau này, đồng thời giúp phía công ty có thể biết được hiệu quả của từng chủ đề/phần trình bày. Rất mong mọi người dành 1 chút thời gian để hoàn thành bảng khảo sát nhé
           </p>   
           </div>    
          </Container>
        </FitHeightContainer>
      </Page>
    );
  }
}


