import React, { Component } from 'react'


import { Container, FitHeightContainer, Page } from 'app/components'
import { connect } from 'react-redux';
import {actions} from 'app/store/actions';
import "./style.scss"
@connect(state=>({
  topicsStore: state.topics
}),{
  getTopicDetails:actions.getTopicDetails
})

class TopicDetails extends Component {
 componentDidMount(){
   this.props.getTopicDetails({
     id:this.props.match.params.id
   })
 }

  render() {
 //const {match} = this.props
 //console.log(match.params.id)
 const {topicsStore}= this.props
 //currentTopic?.name : cho phep lay bien null
 console.log(topicsStore)
 
    return (
      <div className="topicDetail">
        <div className="field">
           <p className="label">Name</p>
           <p className="value">{topicsStore.currentTopic?.name}</p>
        </div>
        <div className="field">
            <p className="label">description</p>
            <p className="value">{topicsStore.currentTopic?.description}</p>
        </div>
        <div className="field">
           <p className="label">author</p>
           <p className="value">{topicsStore.currentTopic?.author}</p>
        </div>
        <div className="field">
           <p className="label">Rating</p>
           <p className="value">{topicsStore.currentTopic?.rating}</p>
        </div>
        <img src={topicsStore.currentTopic?.image} alt=""/>
      </div>
     
    )
  }
}

export default TopicDetails
