import React, { Component } from "react";
import { Table, Button, Popconfirm, Icon, message, Tag, Pagination } from "antd";
import { connect } from "react-redux";
import "./style.scss";

import AddUserForm from './add-users-form'
import EditUserModal from './edit-user-modal'
import { TYPES } from 'app/store/actions'

// import DeleteUserModal from './delete-user-modal'

import { Container, FitHeightContainer, Page } from "app/components";

import { actions } from "app/store/actions";

const PAGE_SIZE = 5

@connect(state=>({ 
  usersStore:state.users  //store
}),{
  getAllUsers:actions.getAllUsers, //action
  createUsers:actions.createUsers,
  updateUsers:actions.updateUsers,
  deleteUsers:actions.deleteUsers
})

export default class TopicsWithRedux extends Component {

/**
 * delete
 */
constructor(){
  super()
  this._currentPage = 1
}
confirm = (e) => {
  const { deleteUsers,getAllUsers, usersStore  } = this.props
  console.log(e.id)
  deleteUsers(e.id, (action) => {
    if (action === TYPES.DELETE_USERS_SUCCESS) {
      if(usersStore.users.content?.length === 1) {
        this._currentPage = this._currentPage - 1; 
      }
      console.log(this._currentPage);
      getAllUsers({
        page: this._currentPage,
        pageSize: PAGE_SIZE
      })
      message.success('Successfully delete users!')
    } else {
      message.error('err delete users!')
    }
  })
  
}

 cancel = (e) => {
  console.log(e)
  message.error('Delete failed!')
}
/**
 * load data
 */
  componentDidMount() {
    this.props.getAllUsers({
      page: 1,
      pageSize: PAGE_SIZE
    })   
  }

  _onPageChange = (page) => {
    this._currentPage = page
    this.props.getAllUsers({
      page,
      pageSize: PAGE_SIZE
    })
  }

  render() {
    const { createUsers, usersStore, updateUsers, deleteUsers, getAllUsers } = this.props
    const { users, loading } = this.props.usersStore; 
    const currentPage=this._currentPage
    console.log(usersStore)
    const columns = [
      {
        title: "USER NAME",
        dataIndex: "username",
        key: "username"
      },
      {
        title: "EMAIL",
        dataIndex: "email",
        key: "email"
      },
      {
        title: "MAJOR",
        dataIndex: "major",
        key: "major"
      },
      {
        title: "ROLE",
        dataIndex: "role",
        key: "role" ,
         render: row => (row === 1 ?
          <Tag color='volcano' key={1}>
            ADMIN
          </Tag> 
            : 
          <Tag color='green' key={2}>
            USER
          </Tag>
        )
       
      },
      {
        title: 'ACTION',
        key: 'action',
        render: (row, record) => (
           <span>
             <Button
               type="ghost"
               onClick={() => {
                 this._editUserModal.open(record)
               }}
             ><Icon type="edit" /> 
             </Button>
 
             <Popconfirm
               title="Are you sure delete this user?"
               onConfirm={() => this.confirm(record)}
               onCancel={this.cancel}
               okText="Yes"
               cancelText="No"
             >
               <a href="#"><Button type="danger"><Icon type="delete" /></Button></a>
            </Popconfirm>

           </span>
        )
      }
    ];

    return (
      <Page className="home">
        <FitHeightContainer hasHeader>
          <Container>
            <h2>USER MANAGEMENT</h2>
            <br/>
            <Button
              type="primary"
              onClick={() => {
                this._addUserModal.open()
              }}
            ><Icon type="user-add" />
              Add User
            </Button> 

            <br/>  

           {/* <AddUsersForm/> */}
            <Table
              rowKey={(row, index) => index} //fix lỗi key  bằng cách tạo ra các index ++ ở các dòng
              pagination={false}
              loading={loading}        
              columns={columns}
              dataSource={usersStore.users.content}
            />

          
            <Pagination
              current={currentPage}
              pageSize={PAGE_SIZE}
              total={usersStore.users.totalElements}
              onChange={this._onPageChange}
            />


             <AddUserForm
             usersStore={usersStore}
             createUsers={createUsers}
             getAllUsers={getAllUsers}
              ref={(ref) => { this._addUserModal = ref }}
            /> 

              <EditUserModal
               updateUsers={updateUsers}
               getAllUsers={getAllUsers}
               ref={(ref) => { this._editUserModal = ref }}
             />
            
          </Container>
        </FitHeightContainer>
      </Page>
    );
  }
}

