import React, { Component } from 'react'
import { Form, Input, Button, message } from 'antd'
import { connect } from 'react-redux'

import request from './request'

class UserForm extends Component {
  constructor(props) {
    super(props)

    this.state = {
      currentUser: null,

      id: '',
      name: '',
      age: '',
      phone: ''
    }
  }

  async componentDidMount() {
    const response = await request()
    const result = await response.text()

    this.setState({
      users: JSON.parse(result)
    })
  }

  _onUserSelected = (user) => {
    this.setState({
      currentUser: user,
      name: user.name,
      age: user.age,
      phone: user.phone
    })
  }

  _onAddUser = () => {
    const { users, name, age, phone, id } = this.state

    if (name !== '' && age !== '' && phone !== '' && id !== '') {
      const user = {
        id,
        name,
        age,
        phone
      }
      this.props.addUser(user)

      this.setState({
        id: '',
        name: '',
        phone: '',
        age: ''
      })

    } else {
      message.error('Please fill all the fields')
    }
  }

  _onUpdateUser = () => {
    const { currentUser, name, age, phone, id } = this.state

    this.props.updateUser({
      id: currentUser.id,
      name,
      age,
      phone
    })

    this.setState({
      currentUser: null,
      id: '',
      name: '',
      phone: '',
      age: ''
    })
  }

  _onDeleteUser = () => {
    const { users, currentUser } = this.state

    const newUsers = users.filter((user) => {
      return user.id !== currentUser.id
    })

    this.setState({
      users: newUsers,
      currentUser: null,

      id: '',
      name: '',
      phone: '',
      age: ''
    })
  }

  _onInputChange = (field) => (e) => {
    this.setState({
      [field]: e.target.value
    })
  }

  render() {
    const { currentUser, id, name, age, phone } = this.state
    const { users } = this.props
    // console.log('USERS', users);

    return (
      <div className='container'>
        <Form>
          <Form.Item>
            <Input placeholder="ID" type="number" value={id} onChange={this._onInputChange('id')} />
          </Form.Item>
          <Form.Item>
            <Input placeholder="Name" type="text" value={name} onChange={this._onInputChange('name')} />
          </Form.Item>
          <Form.Item>
            <Input placeholder="Age" type="text" value={age} onChange={this._onInputChange('age')} />
          </Form.Item>
          <Form.Item>
            <Input placeholder="Phone" type="number" value={phone} onChange={this._onInputChange('phone')} />
          </Form.Item>
          {!currentUser && (
            <Button type="primary" onClick={this._onAddUser}>Add</Button>
          )}
          {currentUser && (
            <Button onClick={this._onUpdateUser}>Update</Button>
          )}
          {currentUser && (
            <Button type="danger" onClick={this._onDeleteUser}>Delete</Button>
          )}
        </Form>
        {/* <UserList users={users} onUserSelected={this._onUserSelected} /> */}

        
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  users: state.users
})

const mapDispatchToProps = (dispatch) => ({
  addUser: (user) => dispatch({ type: 'ADD_USER', user: user }),
  updateUser: (user) => dispatch({ type: 'UPDATE_USER', user: user }),
  deleteUser: (id) => dispatch({ type: 'DELETE_USER', id: id }),
})

export default connect(mapStateToProps, mapDispatchToProps)(UserForm)
