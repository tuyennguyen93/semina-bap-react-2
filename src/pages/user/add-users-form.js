import React, { Component } from 'react'
import { Input, Button, Modal, Select, Option, message } from 'antd'
import { Formik, Field, Form } from 'formik'
import * as yup from 'yup'
import { TYPES } from 'app/store/actions'

const schemaValidation = yup.object().shape({
  name: yup.string().required('name là bắt buộc'),
  username: yup.string().required(),
  email: yup.string().email().required(),
  password: yup.string().required(),
  role: yup.number().required(),
  major: yup.string().required()
})

export default class AddUserForm extends Component {
    // modal
    state = {
      visible: false

    }

      open = (user) => {
        this.setState({
          visible: true
        })
      };

      close = (e) => {
        this.setState({
          visible: false
        })
      }

      // event
   _onSubmit = (value) => {
     const { createUsers, getAllUsers, usersStore } = this.props
     createUsers(value, (action) => {
       if (action === TYPES.CREATE_USERS_SUCCESS) {
         getAllUsers()
         message.success("create user success!")
       } else{
        message.error("UserName or email Already Exist!,created student not success!")
       }
     })
     console.log(usersStore)
     this.close()
     console.log(value)
   }

   render() {
     return (
            <div>
                <Modal
                  title="Add user"
                  visible={this.state.visible}
                  onCancel={this.close}
                  onOk={this._onSubmit}
                  footer={null} // ko muốn hiện footer
                >
                    <Formik

                      validationSchema={schemaValidation}
                      onSubmit={this._onSubmit}

                      render={({ handleSubmit }) => (
                            <Form>
                            {/* name */}
                            <div className="field">
                                    <p className="label">name</p>
                                    <Field
                                      name="name"
                                      render={({ field, form }) => (
                                        <div>
                                        <Input {...field} />
                                        <p className="error-message">{form.touched.name && form.errors.name}</p>
                                        </div>
                                      )}
                                    />
                            </div>
                            {/* username */}
                            <div className="field">
                                    <p className="label">username</p>
                                    <Field
                                      name="username"
                                      render={({ field, form }) => (
                                        <div>
                                        <Input {...field} />
                                        <p className="error-message">{form.touched.username && form.errors.username}</p>
                                        </div>
                                      )}
                                    />
                            </div>
                            {/* email */}
                            <div className="field">
                                    <p className="label">email</p>
                                    <Field
                                      name="email"
                                      render={({ field, form }) => (
                                        <div>
                                        <Input {...field} />
                                        <p className="error-message">{form.touched.email && form.errors.email}</p>
                                        </div>
                                      )}
                                    />
                            </div>
                            {/* pass */}
                            <div className="field">
                                    <p className="label">password</p>
                                    <Field
                                      name="password"
                                      render={({ field, form }) => (
                                        <div>
                                        <Input {...field} />
                                        <p className="error-message">{form.touched.password && form.errors.password}</p>
                                        </div>
                                      )}
                                    />
                            </div>
                            {/* role */}
                           <Field
                             name="role"
                             render={({ field }) => (
                          <div>
                            <p className="label">role</p>
                            <Select
                              {...field}
                              onChange={(value) => {
                                field.onChange({ target: { value, name: field.name } })
                              }}
                              onBlur={(value) => {
                                field.onChange({ target: { value, name: field.name } })
                              }}
                            >
                              <Select.Option value={0}>User</Select.Option>
                              <Select.Option value={1}>Admin</Select.Option>
                            </Select>
                          </div>
                             )}
                           />
                            {/* major */}
                            <div className="field">
                                    <p className="label">major</p>
                                    <Field
                                      name="major"
                                      render={({ field, form }) => (
                                        <div>
                                        <Input {...field} />
                                        <p className="error-message">{form.touched.major && form.errors.major}</p>
                                        </div>
                                      )}
                                    />
                            </div>
                            <br />
                            <Button type="primary" onClick={handleSubmit}>add</Button>
                            </Form>
                      )}
                    />
                </Modal>
            </div>
     )
   }
}
