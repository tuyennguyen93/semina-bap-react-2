import React from 'react'
import { Modal, Button, Input, Row, Col, Select, message } from 'antd'
import { Formik, Field, Form } from 'formik'
import * as yup from 'yup'
import { TYPES } from 'app/store/actions'

const schemaValidation = yup.object().shape({
  name: yup.string().required('name là bắt buộc'),
  username: yup.string().required(),
  email: yup.string().email().required(),
  password: yup.string().required(),
  role: yup.number().required(),
  major: yup.string().required()
})

class addUserModal extends React.Component {
  state = {
    visible: false,
    user: null
  }

  open = (user) => {
    this.setState({
      visible: true,
      user
    })
  };

  close = (e) => {
    this.setState({
      visible: false,
      user: null
    })
  }

  _onSelectChange = (value) => {
    console.log(value)
  }

  _submit = () => {
    this.close()
    console.log('aaa')
  }

  // event
  _onSubmit = (value) => {
    const { updateUsers, getAllUsers } = this.props
    updateUsers(value, (action) => {
      console.log(action);
      if (action === TYPES.UPDATE_USERS_SUCCESS) {
        this.close()
        getAllUsers()
       // message.success('update success!')
      } else{
        this.close()
        getAllUsers()
      }
    })


    // this.close()
    // console.log(value)
  }

  render() {
    const { user } = this.state
    console.log(user)
    // fix trung data o modal
    const initialValues = user || {
      email: '',
      username: '',
      password: '',
      role: '',
      major: ''
    }
    return (
      <div>
          <Modal
            title="Chỉnh Sửa User"
            visible={this.state.visible}
            onCancel={this.close}
            onOk={this._onSubmit}
            footer={null} // ko muốn hiện footer
          >
              <Formik
                enableReinitialize
                initialValues={initialValues}
                validationSchema={schemaValidation}
                onSubmit={this._onSubmit}

                render={({ handleSubmit }) => (
                      <Form>
                      {/* name */}
                      <div className="field">
                              <p className="label">name</p>
                              <Field
                                name="name"
                                render={({ field, form }) => (
                                  <div>
                                  <Input {...field} />
                                  <p className="error-message">{form.touched.name && form.errors.name}</p>
                                  {/* fix hiện Lỗi sau khi nhập dữ liệu */}
                                  </div>
                                )}
                              />
                      </div>
                      {/* username */}
                      <div className="field">
                              <p className="label">username</p>
                              <Field
                                name="username"
                                render={({ field, form }) => (
                                  <div>
                                  <Input {...field} disabled />
                                  <p className="error-message">{form.touched.username && form.errors.username}</p>
                                  </div>
                                )}
                              />
                      </div>
                      {/* email */}
                      <div className="field">
                              <p className="label">email</p>
                              <Field
                                name="email"
                                render={({ field, form }) => (
                                  <div>
                                  <Input {...field} disabled />
                                  <p className="error-message">{form.touched.email && form.errors.email}</p>
                                  </div>
                                )}
                              />
                      </div>
                      {/* pass */}
                      <div className="field">
                              <p className="label"></p>
                              <Field
                                name="password"
                                render={({ field, form }) => (
                                  <div>
                                  <Input {...field} type="password" hidden />
                                  <p className="error-message">{form.touched.password && form.errors.password}</p>
                                  </div>
                                )}
                              />
                      </div>
                      {/* role */}


                      <Field
                        name="role"
                        render={({ field }) => (
                    <div>
                      <p className="label">role</p>
                      <Select
                        {...field}
                        onChange={(value) => {
                          field.onChange({ target: { value, name: field.name } })
                        }}
                        onBlur={(value) => {
                          field.onChange({ target: { value, name: field.name } })
                        }}
                      >
                        <Select.Option value={0}>User</Select.Option>
                        <Select.Option value={1}>Admin</Select.Option>
                      </Select>
                    </div>
                        )}
                      />
                      {/* major */}
                      <div className="field">
                              <p className="label">major</p>
                              <Field
                                name="major"
                                render={({ field, form }) => (
                                  <div>
                                  <Input {...field} />
                                  <p className="error-message">{form.touched.major && form.errors.major}</p>
                                  </div>
                                )}
                              />
                      </div>
                      <Button type="primary" onClick={handleSubmit}>Update</Button>
                      <Button type="default" onClick={() => this.close()}>cancel</Button>
                      </Form>
                )}
              />
          </Modal>
      </div>
    )
  }
}

export default addUserModal
