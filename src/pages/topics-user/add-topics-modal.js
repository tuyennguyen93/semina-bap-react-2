import React from 'react'
import { Modal, Button, Input, Row, Col, Select, Avatar, message } from 'antd'
import { Formik, Field, Form } from 'formik'
import * as yup from 'yup'
import { TYPES } from 'app/store/actions'

const { Option, TextArea } = Select

const schemaValidation = yup.object().shape({
  title: yup.string(),
  description: yup.string(),
  category: yup.string()
})

class addUserModal extends React.Component {
  state = {
    visible: false,
    user: null
  }

  open = (user) => {
    this.setState({
      visible: true,
      user
    })
  };

  close = (e) => {
    this.setState({
      visible: false,
      user: null
    })
  }

  _onSelectChange = (value) => {
    console.log(value)
  }

  _submit = () => {
    this.close()
    console.log('aaa')
  }

  // event
  _onSubmit = (values) => {
    values = {
      ...values,
      categories: values.category.map(item => ({
        id: item,
        name: this.props.categories.find(cate => cate.id === item).name
      }))
    }
    console.log(values.category)
    const { createTopics } = this.props
    createTopics(values, (action) => {
      if (action === TYPES.CREATE_TOPICS_SUCCESS) {
        this.close()
        message.success('create topics success')
      }
    })
  }

  render() {
    const { user } = this.state
    const { categories } = this.props
    // console.log(categories)
    // fix trung data o modal
    const initialValues = user || {
      title: '',
      description: '',
      category: ''
    }
    return (
      <div>
          <Modal
            title="Add new topics "
            visible={this.state.visible}
            onCancel={this.close}
            onOk={this._onSubmit}
            footer={null} // ko muốn hiện footer
          >
              <Formik
                enableReinitialize
                initialValues={initialValues}
                validationSchema={schemaValidation}
                onSubmit={this._onSubmit}

                render={({ handleSubmit }) => (
                      <Form>
                        {/* Title */}
                        <div className="field">
                                <p className="label">Title</p>
                                <Field
                                  name="title"
                                  render={({ field, form }) => (
                                    <div>
                                    <Input {...field} />
                                    <p className="error-message">{form.touched.title && form.errors.title}</p>
                                    {/* fix hiện Lỗi sau khi nhập dữ liệu */}
                                    </div>
                                  )}
                                />
                        </div>

                        {/* description */}
                        <div className="field">
                                <p className="label">Description</p>
                                <Field
                                  name="description"
                                  render={({ field, form }) => (
                                    <div>
                                    <Input.TextArea
                                      {...field}
                                      placeholder="write description for this topic"
                                      autosize={{ minRows: 2, maxRows: 6 }}
                                    />
                                    <p className="error-message">{form.touched.description && form.errors.description}</p>
                                    </div>
                                  )}
                                />
                        </div>

                         {/* type */}
                         <Field
                           name="type"
                           render={({ field }) => (
                          <div>
                            <p className="label">Type</p>
                            <Select
                              {...field}
                              onChange={(value) => {
                                field.onChange({ target: { value, name: field.name } })
                              }}
                              onBlur={(value) => {
                                field.onChange({ target: { value, name: field.name } })
                              }}
                            >
                              <Select.Option value={0}>Suggest</Select.Option>
                              <Select.Option value={1}>Speaker</Select.Option>
                            </Select>
                          </div>
                           )}
                         />


                      {/* category */}
                      <Field
                        name="category"
                        render={({ field }) => (
                          <div>
                            <p className="label">category</p>
                            <Select
                              mode="multiple"
                              onChange={(value) => {
                                field.onChange({ target: { value, name: field.name } })
                              }}
                              onBlur={(value) => {
                                field.onChange({ target: { value, name: field.name } })
                              }}
                            >
                             {categories.map(item => (
                                <Select.Option value={item.id} key={item.id}>
                                  {item.name}
                                </Select.Option>
                             ))}
                            </Select>
                          </div>
                        )}
                      />

                      <Button type="primary" onClick={handleSubmit}>Add</Button>
                      <Button type="default" onClick={() => this.close()}>Cancel</Button>
                      </Form>
                )}
              />
          </Modal>
      </div>
    )
  }
}

export default addUserModal
