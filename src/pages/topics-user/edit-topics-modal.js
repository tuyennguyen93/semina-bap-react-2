import React from 'react'
import { Modal, Button, Input, Row, Col, Select, Tag, message } from 'antd'
import { Formik, Field, Form } from 'formik'
import * as yup from 'yup'
import { TYPES } from 'app/store/actions'

const schemaValidation = yup.object().shape({
  title: yup.string(),
  description: yup.string(),
  category: yup.string()
})

class addUserModal extends React.Component {
  state = {
    visible: false,
    user: null
  }

  open = (user) => {
    this.setState({
      visible: true,
      user
    })
  };

  close = (e) => {
    this.setState({
      visible: false,
      user: null
    })
  }

  _onSelectChange = (value) => {
    console.log(value)
  }

  _submit = () => {
    this.close()
    console.log('aaa')
  }

  // event
  _onSubmit = (value) => {
    // this.close()
    // console.log(value)
  }

  render() {
    const { user } = this.state
    console.log(user)
    // fix trung data o modal
    const initialValues = user || {
      email: '',
      username: '',
      password: '',
      role: '',
      major: ''
    }
    return (
      <div>
          <Modal
            title=" Topics Detail"
            visible={this.state.visible}
            onCancel={this.close}
            onOk={this._onSubmit}
            footer={null} // ko muốn hiện footer
          >
              <Formik
                enableReinitialize
                initialValues={initialValues}
                validationSchema={schemaValidation}
                onSubmit={this._onSubmit}

                render={({ handleSubmit }) => (
                      <Form>
                      {/* name */}
                      <div className="field">
                              <p className="label">Title</p>
                              <Field
                                name="title"
                                render={({ field, form }) => (
                                  <div>
                                  <Input {...field} disabled />
                                  <p className="error-message">{form.touched.title && form.errors.title}</p>
                                  {/* fix hiện Lỗi sau khi nhập dữ liệu */}
                                  </div>
                                )}
                              />
                      </div>

                       {/* description */}
                       <div className="field">
                              <p className="label">Description</p>
                              <Field
                                name="description"
                                render={({ field, form }) => (
                                  <div>
                                  <Input {...field} disabled />
                                  <p className="error-message">{form.touched.description && form.errors.description}</p>
                                  {/* fix hiện Lỗi sau khi nhập dữ liệu */}
                                  </div>
                                )}
                              />
                       </div>

                       {/* type */}
                       <Field
                         name="type"
                         render={({ field }) => (
                          <div>
                            <p className="label">Type</p>
                            <Select
                              {...field}
                              onChange={(value) => {
                                field.onChange({ target: { value, name: field.name } })
                              }}
                              onBlur={(value) => {
                                field.onChange({ target: { value, name: field.name } })
                              }}
                            >
                              <Select.Option value={0}>Suggest</Select.Option>
                              <Select.Option value={1}>Speaker</Select.Option>
                            </Select>
                          </div>
                         )}
                       />

                         {/* status */}
                         <Field
                           name="status"
                           render={({ field }) => (
                          <div>
                            <p className="label">Status</p>
                            <Select
                              {...field}
                              onChange={(value) => {
                                field.onChange({ target: { value, name: field.name } })
                              }}
                              onBlur={(value) => {
                                field.onChange({ target: { value, name: field.name } })
                              }}
                            >
                              <Select.Option value={1}> <Tag color="green">Approve</Tag> </Select.Option>
                              <Select.Option value={0}> <Tag color="volcano">Un Upprove</Tag> </Select.Option>
                            </Select>
                          </div>
                           )}
                         />
                      <Button type="default" onClick={() => this.close()}>Ok</Button>
                      </Form>
                )}
              />
          </Modal>
      </div>
    )
  }
}

export default addUserModal
