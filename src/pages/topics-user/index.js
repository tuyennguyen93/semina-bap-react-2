import React, { Component } from 'react'
import { Table, Radio, Button, Popconfirm, Icon, message, Tag, Tabs } from 'antd'
import { connect } from "react-redux";
import './style.scss'
import { actions, TYPES } from "app/store/actions";
import { Container, FitHeightContainer, Page } from 'app/components'
import AddTopicsModal from './add-topics-modal'
import EditTopicsModal from './edit-topics-modal'
import ViewUserModal from './view-user-modal'
//import Edit

const { TabPane } = Tabs

@connect(state=>({ 
  topicsStore:state.topics,
  categoriesStore:state.categories 
}),{
  getAllTopics:actions.getAllTopics,
  getAllApproveTopics:actions.getAllApproveTopics,
  getAllDenyTopics:actions.getAllDenyTopics,
  getAllMineTopics:actions.getAllMineTopics,
  getAllCategories:actions.getAllCategories,
  createTopics:actions.createTopics,
  updateTopics:actions.updateTopics,
  deleteTopics:actions.deleteTopics,
  getUserById:actions.getUserById
})

export default class Home extends Component {

  state = {
    topics: [],
    topicsApprove:[],
    topicsUnApprove:[],
    mineTopics:[],
    isLoading: false
  }

  async componentDidMount() {
    //this.props.getAllTopics()
    this.props.getAllApproveTopics()
    this.props.getAllDenyTopics()
    this.props.getAllMineTopics()
    this.props.getAllCategories()
  }

  getList = (value) => {
    const { getUserById, profileStore } = this.props
    getUserById(value, (action, data) => {
      console.log(data)
      if(action === TYPES.GET_USER_BY_ID_SUCCESS) {   
        this._viewUserModal.open(data)
     }
    })
  }

  render() {
    const { topicsApprove, topicsUnApprove, mineTopics, isLoading } = this.props.topicsStore
    const {createTopics, updateTopics, getUserById}= this.props
    const {categories}= this.props.categoriesStore
    // console.log(topics)
    // console.log(topicsApprove)
    // console.log(topicsUnApprove)
    // console.log(categories)
    // colume topic approve
    const columnsApprove = [
      {
        title: 'title',
        dataIndex: 'title',
        key: 'title'
      },{
        title: 'description',
        dataIndex: 'description',
        key: 'description',
        render: (row, record) => {
          var description = record.description
          if(description.length>10){
            description = description.substr(0,10)+ "..."  
          }   
         return description
        }
      },{
        title: 'category',
        dataIndex: 'categories',
        key: 'categories',
        render: category1 => (
          <span>
            {
              category1.map(category2 => (<Tag color="red" key={category2.id}>{category2.name}</Tag>))
            }
          </span>
        )
      },{
        title: 'Author',
        key: 'userId',
        render:(row,record)=>(
          <span>
             {/* view user button */}
             <Button 
               id="view"
               type="link"
               onClick={() =>
                  this.getList(record.userId)
               }
             ><Icon type="eye" />
             </Button>
          </span>
        )
      },{
        title: 'ACTION',
        key: 'action',
        render: (row, record) => (
           <span>

           {/* edit button */}
             <Button
               type="primary"
               onClick={() => {
                 this._editTopicsModal.open(record)
               }}
             ><Icon type="down-circle" />
             </Button>
             
           </span>
        )
      }

    ]

    // colume topic unapprove
    const columnsUnApprove = [
      {
        title: 'title',
        dataIndex: 'title',
        key: 'title'
      },{
        title: 'description',
        dataIndex: 'description',
        key: 'description',
        render: (row, record) => {
          var description = record.description
          if(description.length>10){
            description = description.substr(0,10)+ "..."  
          }   
         return description
        }
      },{
        title: 'category',
        dataIndex: 'categories',
        key: 'categories',
        render: category1 => (
          <span>
            {
              category1.map(category2 => (<Tag color="red" key={category2.id}>{category2.name}</Tag>))
            }
          </span>
        )
      }

    ]

    // colume mine topic 
    const columnsMineTopics = [
      {
        title: 'title',
        dataIndex: 'title',
        key: 'title'
      },{
        title: 'description',
        dataIndex: 'description',
        key: 'description',
        render: (row, record) => {
          var description = record.description
          if(description.length>10){
            description = description.substr(0,10)+ "..."  
          }   
         return description
        }
      },{
        title: 'category',
        dataIndex: 'categories',
        key: 'categories',
        render: category1 => (
          <span>
            {
              category1.map(category2 => (<Tag color="red" key={category2.id}>{category2.name}</Tag>))
            }
          </span>
        )
      }

    ]
    //event click of tab
    function callback(key) {
      console.log(key);
    }
    return (
      <Page className="home">
        <FitHeightContainer hasHeader>
          <Container>
            <h2>Topic List</h2>
            <br/>

            <Button type="primary"
              onClick={() => {
                this._addTopicsModal.open()
              }}
            ><Icon type="file-add" />Suggest Topic</Button>
              <Tabs defaultActiveKey="1" onChange={callback}>
                {/* Approve Topics */}
                <TabPane tab="Topics Approve" key="1">
                  
                  <Table rowKey={(row, index) => index} pagination={true} loading={isLoading} dataSource={topicsApprove} columns={columnsApprove} />
                </TabPane>

                {/* UnApprove Topics */}
                <TabPane tab="Topics UnApprove" key="2">
                  <Table rowKey={(row, index) => index} pagination={true} loading={isLoading} dataSource={topicsUnApprove} columns={columnsUnApprove} />          
                </TabPane>

                {/* All Topics */}
                <TabPane tab="My Topics" key="3">

                  <Table rowKey={(row, index) => index} pagination={true} loading={isLoading} dataSource={mineTopics} columns={columnsMineTopics} />
                </TabPane>

              </Tabs>

              <AddTopicsModal
                categories = {categories}
                createTopics = {createTopics}
                ref={(ref) => { this._addTopicsModal = ref }}
            /> 

              <EditTopicsModal
                categories = {categories}
                updateTopics = {updateTopics}
                ref={(ref) => { this._editTopicsModal = ref }}
              /> 

              <ViewUserModal
                  getUserById={getUserById}
                 ref={(ref) => { this._viewUserModal = ref }}
              />

          </Container>
        </FitHeightContainer>
      </Page>
    )
  }
}
