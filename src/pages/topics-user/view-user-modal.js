import React from 'react'
import { Modal, Button, Input, Row, Col, Select, Avatar, message, DatePicker, Table } from 'antd'
import * as yup from 'yup'
import { TYPES } from 'app/store/actions'

class addUserModal extends React.Component {
  state = {
    visible: false,
    profile: null
  }

  open = (profile) => {
    this.setState({
      visible: true,
      profile
    })
  };

  close = (e) => {
    this.setState({
      visible: false,
      profile: null
    })
  }


  // event

  render() {
    const { profile, loading } = this.state
    console.log(profile)
  
    return (
      <div>
          <Modal
            title="Author Detail"
            visible={this.state.visible}
            onCancel={this.close}
            onOk={this._onSubmit}
            footer={null} // ko muốn hiện footer
          >
            UserName: {profile?.name} <br/>
            Email: {profile?.email} <br/>
            Major: {profile?.major} <br/>
              
            <Button type="primary" onClick={() => this.close()}>Ok</Button>
          </Modal>
      </div>
    )
  }
}

export default addUserModal
