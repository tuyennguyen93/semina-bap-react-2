import React, { Component } from 'react'
import { Input, Button, message, Icon } from 'antd'
import { Formik, Field } from 'formik'
import * as yup from 'yup'
// import './style.scss'
import './style.css'

import { Page } from 'app/components'
import Storage from 'app/utils/storage'
import { login } from 'app/api/auth'

// import ảnh
import styled from 'styled-components'

const Logo = styled.img`
  width: 200px;
  margin: 0 auto;
  margin-left: 111px;
  transition: 0.5s;
  &:hover {
    opacity: 0.5;
  }

  &:active {
    width: 150px;
  }
`

const schemaValidation = yup.object().shape({
  username: yup.string().required(),
  password: yup.string().required()
})

class Login extends Component {
  state = {
    isLoading: false
  };

  componentDidMount() {
    document.title = 'SEMINAR-BAP'
  }

  _onSubmit = async (values) => {
    try {
      this.setState({
        isLoading: true
      })

      const result = await login(values)
      // console.log(result.accessToken)
      this.setState({
        isLoading: false
      })

      const { history } = this.props

      Storage.set('ACCESS_TOKEN', `Bearer ${result.accessToken}`)
      Storage.set('IS_ADMIN', result.role === 1)
      history.push('/')
      message.warning('Welcome to website BAP!')
    } catch (error) {
      if (error.status === 500) {
        message.error('Email or password incorrect!')
        this.setState({
          isLoading: false
        })
      }
    }
  };

  render() {
    const { isLoading } = this.state

    // xử lý handsubmit(yup) trước khi xử lý onsubmit
    return (
      <Page className="login" id="shipper">
      <div className="wrapper fadeInDown" >
   <div id="formContent">

   <h2 className="active"> Sign In </h2>
    <h2 className="inactive underlineHover">Sign Up </h2>


    <div className="fadeIn first">
      <img src="https://gitlab-new.bap.jp/uploads/-/system/appearance/logo/1/LogoBAP.png" id="icon" alt="User Icon" />
    </div>

    <Formik
      validationSchema={schemaValidation}
      onSubmit={this._onSubmit}
      render={props => (
            <form className="form" onSubmit={props.handleSubmit}>

                <Field
                  name="username"
                  render={({ field, form }) => (
                    <div>
                      <Input
                      id="login"
                      class="fadeIn second"
                      type="text"
                        prefix={(
                        <Icon
                          type="user"
                          style={{ color: 'rgba(0,0,0,.25)' }}
                        />
                        )}
                        placeholder="Username"
                        {...field}
                      />
                      <p className="error-message">{form.errors.username}</p>
                    </div>
                  )}
                />

                <Field
                  name="password"
                  render={({ field, form }) => (
                    <div>
                      <Input
                      id="login"
                      class="fadeIn second"
                      type="password"
                        prefix={(
                        <Icon
                          type="lock"
                          style={{ color: 'rgba(0,0,0,.25)' }}
                        />
                        )}
                        placeholder="Password"
                        {...field}
                      />
                      <p className="error-message">{form.errors.username}</p>
                    </div>
                  )}
                />
      {/* <Input type="text" id="login" class="fadeIn second" name="login" placeholder="login" /> */}

    
      {/* <Input type="text" id="password" class="fadeIn third" name="login" placeholder="password" /> */}


      <Input  type="submit" class="fadeIn fourth" value="Log In" />
                          
              {/* <Button
                onClick={props.handleSubmit}
                className="button-login"
                type="primary" 
                loading={isLoading}
              >
                Login
              </Button> */}
             
            </form>
          )}
    />

    <div id="formFooter">
      <a className="underlineHover" href="#">Forgot Password?</a>
    </div>

   </div>
      </div>
        {/* <Formik
          validationSchema={schemaValidation}
          onSubmit={this._onSubmit}
          render={props => (
            <form className="form" onSubmit={props.handleSubmit}>
              <Logo src={require('app/resources/images/LogoBAP.png')} />
              <h1>SEMINAR- LOGIN</h1>
              <hr />
              <div className="field">
                <Field
                  name="username"
                  render={({ field, form }) => (
                    <div>
                      <Input
                        prefix={(
                        <Icon
                          type="user"
                          style={{ color: 'rgba(0,0,0,.25)' }}
                        />
                        )}
                        placeholder="Username"
                        {...field}
                      />
                      <p className="error-message">{form.errors.username}</p>
                    </div>
                  )}
                />
                <br />
              </div>
              <div className="field">
                <Field
                  name="password"
                  render={({ field, form }) => (
                    <div>
                      <Input
                        prefix={(
                        <Icon
                          type="lock"
                          style={{ color: 'rgba(0,0,0,.25)' }}
                        />
                        )}
                        type="password"
                        placeholder="Password"
                        {...field}
                        type="password"
                      />
                      <p className="error-message">{form.errors.password}</p>
                    </div>
                  )}
                />
              </div>
              <Button
                onClick={props.handleSubmit}
                className="button-login"
                type="primary"
                loading={isLoading}
              >
                Login
              </Button>
            </form>
          )}
        /> */}
      </Page>
    )
  }
}

export default Login
