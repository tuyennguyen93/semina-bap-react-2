import React, { Component } from "react";
import { Table } from "antd";
import { connect } from "react-redux";
import "./style.scss";

import AddTopicForm from "./add-topic-form";
import { Container, FitHeightContainer, Page } from "app/components";
import { actions } from "app/store/actions";

@connect(
  state => ({
    topicsStore: state.topics
  }),
  {
    getAllTopicss: actions.getAllTopicss
  }
)
export default class TopicsWithRedux extends Component {
    componentDidMount() {
        this.props.getAllTopicss(); //gọi từ index trong action GET_ALL_TOPICS =>getAllTopics
      }
    
  render() {
    const { topics, loading } = this.props.topicsStore;
    const columns = [
      {
        title: "name",
        dataIndex: "name",
        key: "name"
      },
      {
        title: "Description",
        dataIndex: "description",
        key: "description"
      },
      {
        title: "author",
        dataIndex: "author",
        key: "author"
      }
    ];
    return (
        <Page className="home">
          <FitHeightContainer hasHeader>
            <Container>
              <h2>Topic List (dùng Redux1)</h2>
              <AddTopicForm />
              <Table
                rowKey={(row, index) => index} //fix lỗi key  bằng cách tạo ra các index ++ ở các dòng
                pagination={false}
                loading={loading}
                dataSource={topics}
                columns={columns}
                onRow={row => ({
                  onClick: () => {
                    this.props.history.push("/topics-detail/" + row.key);
                    console.log(row);
                  }
                })}
              />
            </Container>
          </FitHeightContainer>
        </Page>
      );
    }
  }
