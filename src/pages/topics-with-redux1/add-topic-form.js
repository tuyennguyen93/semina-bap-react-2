import React, { Component } from "react";
import { Input, Button } from "antd";
import { Formik, Field, Form } from "formik";
import * as yup from "yup";

const schemaValidation = yup.object().shape({
    name: yup.string().required("name là bắt buộc"),
    author: yup.string().required(),
    authorEmail: yup.string().email().required()
  });

import { connect } from "react-redux";
import { actions } from "app/store/actions";

@connect(state=>({ //lấy dữ liệu  state từ reducers 
    topicsStore:state.topics // lấy cái này : topics: [], //lưu giá trị vào đây
  }),{
    createTopics:actions.createTopics //gọi action từ saga 
  })

export default class AddTopicForm extends Component {
  _onSubmit = (values) => {
    const {createTopics}=this.props
    createTopics(values)
    ///this.props.createTopic(values)
    console.log(values);
  };
  render() {
    return (
      <div className="add-topic-form">
        <Formik
          validationSchema={schemaValidation}
          onSubmit={this._onSubmit}
          render={({ handleSubmit }) => (
            <Form>
              <div className="field">
                <p className="label"> name</p>
                <Field
                  name="name"
                  render={({ field, form }) => (
                    <div>
                      <Input {...field} />
                      <p className="error-message">{form.errors.name}</p>
                    </div>
                  )}
                />
              </div>

              <div className="field">
                <p className="label">author</p>
                <Field
                  name="author"
                  render={({ field, form }) => (
                    <div>
                      <Input {...field} />
                      <p className="error-message">{form.errors.author}</p>
                    </div>
                  )}
                />
              </div>

              <div className="field">
                <p className="label">author email</p>
                <Field
                  name="authorEmail"
                  render={({ field, form }) => (
                    <div>
                      <Input {...field} />
                      <p className="error-message">{form.errors.authorEmail}</p>
                    </div>
                  )}
                />
              </div>

              <div className="field">
                <p className="label"> description</p>
                <Field
                  name="description"
                  render={({ field, form }) => (
                    <div>
                      <Input {...field} />
                      <p className="error-message">
                        {" "}
                        {form.errors.description}
                      </p>
                    </div>
                  )}
                />
              </div>
              <Button onClick={handleSubmit}>add</Button>
            </Form>
          )}
        />
      </div>
    );
  }
}
