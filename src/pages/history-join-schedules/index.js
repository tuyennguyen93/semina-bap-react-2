import React, { Component } from "react";
import { Table, Button, Popconfirm, Icon, message, Tag} from "antd";
import { connect } from "react-redux";
import "./style.scss";


import { Container, FitHeightContainer, Page } from "app/components";
import { actions } from "app/store/actions";

@connect(state=>({ 
  registerSchedulesStore:state.registerSchedules
}),{
  getAllByScheduleid:actions.getAllByScheduleid,
  deleteRegisterSchedules:actions.deleteRegisterSchedules 
})

export default class TopicsWithRedux extends Component {

  /**
 * delete
 */

confirm = (e) => {
  const { deleteRegisterSchedules } = this.props
  console.log(e.id)
  deleteRegisterSchedules(e.id)
  message.success('Cancel success!')
  
}

 cancel = (e) => {
  console.log(e)
  message.error('Cancel failed!')
}

  componentDidMount() {
    this.props.getAllByScheduleid(); 
  }

  render() {
    const { registerSchedules, loading } = this.props.registerSchedulesStore; 
    console.log(registerSchedules);
    

    const columns = [
      {
        title: 'No',
        dataIndex: 'no',
        key: 'no',
        render: (row, record, index) => index + 1
      }, {
        title: 'ScheduleId',
        dataIndex: 'scheduleId',
        key: 'scheduleId',     
      }, {
        title: 'Rate',
        dataIndex: 'rate',
        key: 'rate',
      },{
        title: 'Date Join', 
        dataIndex: 'created',
        key: 'created',
      }, {
        title: 'ACTION',
        key: 'action',
        render: (row, record) => (
           <span>
           
             <Popconfirm
               title="Are you sure to cancel this seminar?"
               onConfirm={() => this.confirm(record)}
               onCancel={this.cancel}
               okText="Yes"
               cancelText="No"
             >
               <a href="#"><Button type="danger"><Icon type="delete" /></Button></a>
            </Popconfirm>

           </span>
        )
      }
     
   
      
    ];

    return (
      <Page className="home">
        <FitHeightContainer hasHeader>
          <Container>
            <h2> HISTORY JOIN SCHEDULE</h2>
            <br/>
         

            <br/>  <br/>

            <Table
              rowKey={(row, index) => index} 
              pagination={true}
              loading={loading}
              dataSource={registerSchedules}
              columns={columns}
             
            />

         
          </Container>
        </FitHeightContainer>
      </Page>
    );
  }
}


