import React from 'react'
import { Modal, Button, Input, Row, Col, Select, Avatar, message, DatePicker, Table, Rate } from 'antd'
import * as yup from 'yup'
import { TYPES } from 'app/store/actions'

class addUserModal extends React.Component {
  state = {
    visible: false,
    registerSchedules: null
  }

  open = (registerSchedules) => {
    this.setState({
      visible: true,
      registerSchedules
    })
  };

  close = (e) => {
    this.setState({
      visible: false,
      registerSchedules: null
    })
  }


  // event

  render() {
    const { registerSchedules, loading } = this.state
    console.log(registerSchedules)
    const columns = [
      {
        title: 'userId',
        dataIndex: 'userId',
        key: 'userId'
      }, {
        title: 'userName',
        dataIndex: 'userName',
        key: 'userName'
      }, {
        title: 'rate',
        dataIndex: 'rate',
        key: 'rate',
        render: rate => (
          <span>
            <Rate value={rate} />
          </span>
        )
      }]
    return (
      <div>
          <Modal
            title="List Join Schedule"
            visible={this.state.visible}
            onCancel={this.close}
            onOk={this._onSubmit}
            footer={null} // ko muốn hiện footer
          >

              <Table
                rowKey={(row, index) => index}
                pagination
                loading={loading}
                dataSource={registerSchedules}
                columns={columns}
              />
            <Button type="primary" onClick={() => this.close()}>Close</Button>
          </Modal>
      </div>
    )
  }
}

export default addUserModal
