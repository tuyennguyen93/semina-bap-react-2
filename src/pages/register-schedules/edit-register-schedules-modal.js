import React from 'react'
import { Modal, Button, Input, Row, Col, Select, Tag, message, DatePicker, Rate } from 'antd'
import { Formik, Field, Form } from 'formik'
import * as yup from 'yup'
import { TYPES } from 'app/store/actions'

const desc = ['terrible', 'bad', 'normal', 'good', 'wonderful']

const schemaValidation = yup.object().shape({
  topicId: yup.number(),
  speakerName: yup.string(),
  timeStart: yup.date(),
  timeFinish: yup.date()
})

class addUserModal extends React.Component {
  state = {
    visible: false,
    user: null
  }

  open = (user) => {
    this.setState({
      visible: true,
      user
    })
  };

  close = (e) => {
    this.setState({
      visible: false,
      user: null
    })
  }

  _onSelectChange = (value) => {
    console.log(value)
  }

  _submit = () => {
    this.close()
    console.log('aaa')
  }

  // event
  _onSubmit = (value) => {
    console.log(value)
    const valueObject = {
      schedule_id: value.id,
      rate: value.rate
    }
    // console.log(valueObject);
    const { updateRegisterSchedules , getAllSchedules } = this.props
    updateRegisterSchedules(valueObject, (action) => {
      if (action === TYPES.UPDATE_REGISTER_SCHEDULES_SUCCESS) {
        getAllSchedules()
        this.close()
        message.success('Successfully update rate!')
      } else {
        this.close()
        message.error('Error rate, You have not joined this schedule !')
      }
    })
  }

  render() {
    const { user } = this.state
    console.log(user)
    // fix trung data o modal
    const initialValues = user || {
      name: ''
    }
    return (
      <div>
          <Modal
            title="Rate Speaker"
            visible={this.state.visible}
            onCancel={this.close}
            onOk={this._onSubmit}
            footer={null} // ko muốn hiện footer
          >
              <Formik
                enableReinitialize
                initialValues={initialValues}
                validationSchema={schemaValidation}
                onSubmit={this._onSubmit}

                render={({ handleSubmit }) => (
                      <Form>
                              {/* topicId */}
                              <div className="field">
                                <p className="label">Topic Name</p>
                                <Field
                                  name="topic.title"
                                  render={({ field, form }) => (
                                    <div>
                                    <Input {...field} disabled />
                                    <p className="error-message">{form.touched.topicId && form.errors.topicId}</p>
                                    {/* fix hiện Lỗi sau khi nhập dữ liệu */}
                                    </div>
                                  )}
                                />
                              </div>
                        <br />

                        {/* speakerName */}
                        <div className="field">
                                <p className="label">Speaker Name</p>
                                <Field
                                  name="speakerName"
                                  render={({ field, form }) => (
                                    <div>
                                    <Input {...field} disabled />
                                    <p className="error-message">{form.touched.speakerName && form.errors.speakerName}</p>
                                    {/* fix hiện Lỗi sau khi nhập dữ liệu */}
                                    </div>
                                  )}
                                />
                        </div>
                        <br />

                         {/* time start */}
                         <div className="field">
                                <p className="label">Time start</p>
                                <Field
                                  name="timeStart"
                                  render={({ field, form }) => (
                                    <div>
                                    <Input {...field} disabled />
                                    <p className="error-message">{form.touched.topicId && form.errors.topicId}</p>
                                    {/* fix hiện Lỗi sau khi nhập dữ liệu */}
                                    </div>
                                  )}
                                />
                         </div>
                        <br />

                         {/* time finish */}
                         <div className="field">
                                <p className="label">Time finish</p>
                                <Field
                                  name="timeFinish"
                                  render={({ field, form }) => (
                                    <div>
                                    <Input {...field} disabled />
                                    <p className="error-message">{form.touched.topicId && form.errors.topicId}</p>
                                    {/* fix hiện Lỗi sau khi nhập dữ liệu */}
                                    </div>
                                  )}
                                />
                         </div>
                        <br />

                         {/* rate */}                       
                         <Field
                           name="rate"
                           render={({ field }) => (
                          <div>
                            <p className="label">Rate</p>
                            <Rate
                              {...field}
                            
                              onChange={(value) => {
                                field.onChange({ target: { value, name: field.name } })
                              }}                            
                             />
                          </div>
                           )}
                         />
                        <br />

                      <Button type="primary" onClick={handleSubmit}>Rate</Button>
                      <Button type="default" onClick={() => this.close()}>Cancel</Button>
                      </Form>
                )}
              />
          </Modal>
      </div>
    )
  }
}

export default addUserModal
