import React, { Component } from "react";
import { Table, Button, Popconfirm, Icon, message, Tag} from "antd";
import { connect } from "react-redux";
// import "./style.scss";
import "./style.css";

import ViewUserSchedulesModal from './view-user-register-schedules-modal'
import RateModal from './edit-register-schedules-modal'
import RegisterModal from './register-schedules-modal'


import { Container, FitHeightContainer, Page } from "app/components";
import { actions, TYPES } from "app/store/actions";
@connect(state=>({ //lấy dữ liệu  state từ reducers 
  schedulesStore:state.schedules, // lấy cái này : topics: [], //lưu giá trị vào đây
  topicsStore:state.topics,
  registerSchedulesStore:state.registerSchedules
}),{
  getAllSchedules:actions.getAllSchedules, //gọi action từ saga 
  createSchedules:actions.createSchedules,
  updateSchedules:actions.updateSchedules,
  deleteSchedules:actions.deleteSchedules,
  getAllTopics:actions.getAllTopics,
  createRegisterSchedules:actions.createRegisterSchedules,
  updateRegisterSchedules:actions.updateRegisterSchedules,
  avgRate:actions.avgRate,
  getAllByUserid:actions.getAllByUserid

})

export default class Schedules extends Component {

  /**
 * delete
 */

confirm = (e) => {
  const { deleteSchedules } = this.props
  console.log(e.id)
  deleteSchedules(e.id)
  message.success('Delete success!')
  
}

 cancel = (e) => {
  console.log(e)
  message.error('Delete error!')
}

getList = (value) => {
  const { getAllByUserid, registerSchedulesStore } = this.props
  getAllByUserid(value, (action, data) => {
    if(action === TYPES.GET_ALL_BY_USERID_SUCCESS) {   
      this._viewUserSchedulesModal.open(data)
   }
  })
}

  componentDidMount() {
    this.props.getAllSchedules() 
    this.props.getAllTopics()
  }

  render() {
    const { schedules, loading } = this.props.schedulesStore; 
    const {createSchedules,updateRegisterSchedules,createRegisterSchedules,
      avgRate,getAllSchedules,getAllByUserid} = this.props
    const {topics}= this.props.topicsStore
    const {registerSchedules}= this.props.registerSchedulesStore
  

    const columns = [
     
      {
        title: "SPEAKER",
        dataIndex: "speakerName",
        key: "speakerName"
      },
      {
        title: "TOPIC",
        dataIndex: "topic.title",
        key: "topic.title"
      },
      {
        title: "TIME START",
        dataIndex: "timeStart",
        key: "timeStart"
      },
      {
        title: "TIME FINISH",
        dataIndex: "timeFinish",
        key: "timeFinish"
      },
      {
        title: "AVG-RATE",
        dataIndex: "avgRate",
        key: "avgRate",
        render:(row,record)=>(
          <span>
            <Tag color="magenta">{record.avgRate}</Tag>
          </span>
        )
      },
      {
        title: 'ACTION',
        key: 'action',
        render: (row, record) => (
           <span>
              <Button
               type="primary"
               title="JOIN"
               onClick={() => {
                 this._registerModal.open(record)
               }}
               disabled={record.joiner}><Icon type="check" /> 
             </Button>

             <Button 
               id="rate"
               type="ghost"
               title="RATING"
               onClick={() => {
                 this._rateModal.open(record)
               }}
             ><Icon type="sketch" />
             </Button>

             <Button 
               id="view"
               type="ghost"
               title="VIEW" 
               onClick={() =>
                  this.getList(record.id)
               }
             ><Icon type="eye" />
             </Button>
 

           </span>
        )
      }
    ];

    return (
      <Page className="home">
        <FitHeightContainer>
          <Container>
            <h2>List Schedules</h2>
            <br/>
           

            <br/>  <br/>

            <Table
              rowKey={(row, index) => index} //fix lỗi key  bằng cách tạo ra các index ++ ở các dòng
              pagination={true}
              loading={loading}
              dataSource={schedules}
              columns={columns}
             
            />

            <RateModal
              updateRegisterSchedules={updateRegisterSchedules}
              createRegisterSchedules={createRegisterSchedules}
              getAllSchedules={getAllSchedules}
              avgRate={avgRate}
              ref={(ref) => { this._rateModal = ref }}
            />

            <RegisterModal        
              createRegisterSchedules={createRegisterSchedules}
              getAllSchedules={getAllSchedules}
              topics={topics}
              ref={(ref) => { this._registerModal = ref }}
            />

            <ViewUserSchedulesModal      
              getAllByUserid={getAllByUserid}
              ref={(ref) => { this._viewUserSchedulesModal = ref }}
            />
            
          </Container>
        </FitHeightContainer>
      </Page>
    );
  }
}


