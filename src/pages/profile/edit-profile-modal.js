import React from 'react'
import { Modal, Button, Input, message } from 'antd'
import { Formik, Field, Form } from 'formik'
import * as yup from 'yup'
import { TYPES } from 'app/store/actions'
import './style.css'

const schemaValidation = yup.object().shape({
  name: yup.string().required(),
  password: yup.string().required(),
  major: yup.string().required()
})

class addUserModal extends React.Component {
  state = {
    visible: false,
    user: null
  }

  open = (user) => {
    this.setState({
      visible: true,
      user
    })
  };

  close = (e) => {
    this.setState({
      visible: false,
      user: null
    })
  }

  // event
  _onSubmit = (value) => {
    console.log(value)
    const { updateProfile } = this.props
    updateProfile(value, (action) => {
      if (action === TYPES.UPDATE_PROFILE_SUCCESS) {
        this.close()
        message.success('update profile success!')
      } else {
        message.success('update profile failed!')
      }
    })
  }

  render() {
    const { user } = this.state
    const { profile } = this.props
    console.log(user)
    const initialValues = profile || {
      email: '',
      username: '',
      password: '',
      role: '',
      major: '',
      name: ''
    }
    return (
      <div>
          <Modal
            title="Edit Profile"
            visible={this.state.visible}
            onCancel={this.close}
            onOk={this._onSubmit}
            footer={null} 
          >
              <Formik
                enableReinitialize
                initialValues={initialValues}
                validationSchema={schemaValidation}
                onSubmit={this._onSubmit}

                render={({ handleSubmit }) => (
                      <Form>
                      {/* name */}
                      <div className="field">
                              <p className="label">name</p>
                              <Field
                                name="name"
                                render={({ field, form }) => (
                                  <div>
                                  <Input {...field} />
                                  <p className="error-message">{form.touched.name && form.errors.name}</p>                               
                                  </div>
                                )}
                              />
                      </div>


                      {/* username */}
                      <div className="field">
                              <p className="label">username</p>
                              <Field
                                name="username"
                                render={({ field, form }) => (
                                  <div>
                                  <Input {...field} value={profile.username} disabled />
                                  <p className="error-message">{form.touched.username && form.errors.username}</p>
                                  </div>
                                )}
                              />
                      </div>
                      {/* email */}
                      <div className="field">
                              <p className="label">email</p>
                              <Field
                                name="email"
                                render={({ field, form }) => (
                                  <div>
                                  <Input {...field} value={profile.email} disabled />
                                  <p className="error-message">{form.touched.email && form.errors.email}</p>
                                  </div>
                                )}
                              />
                      </div>

                      {/* major */}
                      <div className="field">
                              <p className="label">major</p>
                              <Field
                                name="major"
                                render={({ field, form }) => (
                                  <div>
                                  <Input {...field} />
                                  <p className="error-message">{form.touched.major && form.errors.major}</p>
                                  </div>
                                )}
                              />
                      </div>
                      <Button type="primary" onClick={handleSubmit} id="btnUpdateModal">Update</Button>
                      <Button type="default" onClick={() => this.close()} id="btnCancelModal">cancel</Button>
                      </Form>
                )}
              />
          </Modal>
      </div>
    )
  }
}

export default addUserModal
