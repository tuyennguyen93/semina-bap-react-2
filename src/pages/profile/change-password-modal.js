import React from 'react'
import { Modal, Button, Input, message } from 'antd'
import { Formik, Field, Form } from 'formik'
import * as yup from 'yup'
import { TYPES } from 'app/store/actions'
import './style.css'

const schemaValidation = yup.object().shape({
  oldPass: yup.string()
    .required()
    .min(8, 'Password is too short - should be 8 chars minimum.')
    .matches(/[a-zA-Z0-9]/, 'Password can only contain Latin letters.'),
  newPass: yup.string()
    .required()
    .min(8, 'Password is too short - should be 8 chars minimum.')
    .matches(/[a-zA-Z0-9]/, 'Password can only contain Latin letters.'),
  confirmPass: yup.string()
    .oneOf(
      [yup.ref('newPass')],
      'Passwords do not match',
    ).required()
    .min(8, 'Password is too short - should be 8 chars minimum.')
    .matches(/[a-zA-Z0-9]/, 'Password can only contain Latin letters.')
})

class addUserModal extends React.Component {
  state = {
    visible: false,
    user: null
  }

  open = (user) => {
    this.setState({
      visible: true,
      user
    })
  };

  close = (e) => {
    this.setState({
      visible: false,
      user: null
    })
  }

  // event
  _onSubmit = (value) => {
    const { changePassword } = this.props
    changePassword(value, (action) => {
      this.close()
      if (action === TYPES.CHANGE_PASSWORD_SUCCESS) {
        message.success('change password success!')
      } else {
        message.error('Old password does not match, change password failed!')
      }
    })
  }

  render() {
    const { user } = this.state
    console.log(user)

    const initialValues = user || {
      email: '',
      username: '',
      password: '',
      role: '',
      major: ''
    }
    return (
      <div>
          <Modal
            title="Change Password"
            visible={ this.state.visible }
            onCancel={this.close}
            onOk={this._onSubmit}
            footer={null} // ko muốn hiện footer
          >
              <Formik
                enableReinitialize
                initialValues={initialValues}
                validationSchema={schemaValidation}
                onSubmit={this._onSubmit}

                render={({ handleSubmit }) => (
                      <Form>

                      {/* old pass */}
                      <div className="field">
                              <p className="label">Old Password</p>
                              <Field
                                name="oldPass"
                                render={({ field, form }) => (
                                  <div>
                                  <Input {...field} type="password" />
                                  <p className="error-message">{form.touched.oldPass && form.errors.oldPass}</p>
                                  </div>
                                )}
                              />
                      </div>

                       {/* new pass */}
                       <div className="field">
                              <p className="label">New Password</p>
                              <Field
                                name="newPass"
                                render={({ field, form }) => (
                                  <div>
                                  <Input {...field} type="password" />
                                  <p className="error-message">{form.touched.newPass && form.errors.newPass}</p>
                                  </div>
                                )}
                              />
                       </div>

                        {/* confirm new pass */}
                        <div className="field">
                              <p className="label">Confirm Password</p>
                              <Field
                                name="confirmPass"
                                render={({ field, form }) => (
                                  <div>
                                  <Input {...field} type="password" />
                                  <p className="error-message">{form.touched.confirmPass && form.errors.confirmPass}</p>
                                  </div>
                                )}
                              />
                        </div>

                      <Button type="primary" onClick={handleSubmit}>Update</Button>
                      <Button type="default" onClick={() => this.close()} id="btnCancelModal">cancel</Button>
                      </Form>
                )}
              />
          </Modal>
      </div>
    )
  }
}

export default addUserModal
