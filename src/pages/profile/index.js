import React, { Component } from "react";
import { Table, Input, Button, Icon } from "antd";
import { connect } from "react-redux";
//import "./style.scss";
import "./style.css"
import { Container, FitHeightContainer, Page } from "app/components";
import { actions } from "app/store/actions";
import EditProfileModal from './edit-profile-modal'
import ChangePasswordModal from './change-password-modal'


@connect(state=>({ 
  usersStore:state.users 
}),{
  getAllUsers:actions.getAllUsers, //action
  createUsers:actions.createUsers,
  updateUsers:actions.updateUsers,
  deleteUsers:actions.deleteUsers,
  getProfile:actions.getProfile,
  updateProfile:actions.updateProfile,
  changePassword:actions.changePassword
})

export default class Home extends Component {

  componentDidMount() {
    this.props.getProfile();  
  }

  render() {
   
    const { createUsers, usersStore, updateUsers, deleteUsers, updateProfile, changePassword } = this.props
    const { loading, profile } = this.props.usersStore; 
     console.log(profile)
    
    return (
      <Page className="home">
        <FitHeightContainer hasHeader>
          <Container>
          <section className="profile">
  <header className="header">
    <div className="details">
      <img src="https://cosy.chat/assets/images/logo.png" alt="John Doe" className="profile-pic"></img>
      <h1 className="heading">{profile?.name}</h1>
      <div className="location">
      <svg width="18" height="18" viewBox="0 0 24 24" fill="currentColor">
        <path d="M12,11.5A2.5,2.5 0 0,1 9.5,9A2.5,2.5 0 0,1 12,6.5A2.5,2.5 0 0,1 14.5,9A2.5,2.5 0 0,1 12,11.5M12,2A7,7 0 0,0 5,9C5,14.25 12,22 12,22C12,22 19,14.25 19,9A7,7 0 0,0 12 ,2Z"></path>
      </svg>
        <p>Hue, VietNam</p>
      </div>
      <div className="stats">
        <div className="col-4">
          <h4>20</h4>
          <p>Reviews</p>
        </div>
        <div className="col-4">
          <h4>10</h4>
          <p>Schedule</p>
        </div>
        <div className="col-4">
          <h4>100</h4>
          <p>Rate</p>
        </div>
      </div>
      <div>
            Email: {profile?.email} <br/>
            Major: {profile?.major} <br/>
      </div>
      <div>
            <Button
              type="primary"
              id="update"
              onClick={()=>{
                this._editProfileModal.open()
              }}
            >
              <Icon type="edit" />Update</Button>
            
            <Button 
               id="changePass"
              onClick={()=>{
                this._changePasswordModal.open()
              }}
            >
              <Icon type="key" />Password</Button>
            <EditProfileModal
              updateProfile={updateProfile}
              profile={profile}
              ref={(ref) => { this._editProfileModal = ref }}
            />
            <ChangePasswordModal
              changePassword={changePassword}
              
              ref={(ref) => { this._changePasswordModal = ref }}
            />
      </div>
    </div>
  </header>
</section>




          </Container>
        </FitHeightContainer>
      </Page>
    )
  }
}
