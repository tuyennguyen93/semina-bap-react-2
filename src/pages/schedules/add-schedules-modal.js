import React from 'react'
import { Modal, Button, Input, Row, Col, Select, Avatar, message, DatePicker, Tag } from 'antd'
import { Formik, Field, Form } from 'formik'
import * as yup from 'yup'
import { TYPES } from 'app/store/actions'

const { Option, TextArea } = Select


const schemaValidation = yup.object().shape({
  topicId: yup.number().required(),
  timeStart: yup.date().required(),
  timeFinish: yup.date().required().when(
    'timeStart',
    (timeStart, schema) => (timeStart && schema.min(timeStart)),
  )
})

class addUserModal extends React.Component {
  state = {
    visible: false,
    user: null
  }

  open = (user) => {
    this.setState({
      visible: true,
      user
    })
  };

  close = (e) => {
    this.setState({
      visible: false,
      user: null
    })
  }


  // event
  _onSubmit = (values) => {
    values = {
      ...values,
      timeStart: values.timeStart.format('YYYY-MM-DD HH:mm'),
      timeFinish: values.timeFinish.format('YYYY-MM-DD HH:mm'),
      topic: { id: values.topicId }
    }
  
    console.log(values)
    const { createSchedules } = this.props
    createSchedules(values, (action) => {
      if (action === TYPES.CREATE_SCHEDULES_SUCCESS) {
        this.close()
      } else {
        this.close()
        message.error('create schedule  error')
      }
    })
  }

  render() {
    const { user } = this.state
    const { topics } = this.props
    console.log(topics)

    // console.log(categories)
    // fix trung data o modal

    const initialValues = user || {
      topicId: '',
      timeStart: '',
      timeFinish: ''
    }
    return (
      <div>
          <Modal
            title="Add new Category "
            visible={this.state.visible}
            onCancel={this.close}
            onOk={this._onSubmit}
            footer={null} // ko muốn hiện footer
          >
              <Formik
                enableReinitialize
                initialValues={initialValues}
                validationSchema={schemaValidation}
                onSubmit={this._onSubmit}

                render={({ handleSubmit }) => (
                      <Form>
                         {/* Topics Name */}
                         <Field
                           name="topicId"
                           render={({ field }) => (
                          <div>
                            <p className="label">Topics</p>
                            <Select
                              {...field}
                              onChange={(value) => {
                                field.onChange({ target: { value, name: field.name } })
                              }}
                              onBlur={(value) => {
                                field.onChange({ target: { value, name: field.name } })
                              }}
                            >
                            {topics.map(item => (
                              <Select.Option value={item.id} key={item.id}>{item.title}</Select.Option>
                            ))}

                            </Select>
                          </div>
                           )}
                         />

                        {/* speakerName */}
                        <div className="field">
                                <p className="label">Speaker Name</p>
                                <Field
                                  name="speakerName"
                                  render={({ field, form }) => (
                                    <div>
                                    <Input {...field} />
                                    <p className="error-message">{form.touched.speakerName && form.errors.speakerName}</p>
                                    {/* fix hiện Lỗi sau khi nhập dữ liệu */}
                                    </div>
                                  )}
                                />
                        </div>


                        {/* timeStart */}
                        <div className="field">
                                <p className="label">TimeStart</p>
                                <Field
                                  name="timeStart"
                                  render={({ field, form }) => (
                                    <div>

                                      <div>
                                        <DatePicker
                                          showTime
                                          placeholder="Select Time"
                                          // value={field.value}
                                          onOk={value => field.onChange({ target: { value, name: field.name } })}
                                        />
                                      <p className="error-message">{form.touched.timeStart && form.errors.timeStart}</p>
                                      {/* fix hiện Lỗi sau khi nhập dữ liệu */}
                                      </div>
                                    </div>
                                  )}
                                />
                        </div>

                        {/* timeFinish */}
                        <div className="field">
                                <p className="label">TimeFinish</p>
                                <Field
                                  name="timeFinish"
                                  render={({ field, form }) => (
                                    <div>

                                      <div>
                                        <DatePicker
                                          showTime
                                          placeholder="Select Time"
                                          // value={field.value}
                                          onOk={value => field.onChange({ target: { value, name: field.name } })}
                                        />
                                      <p className="error-message">{form.touched.timeFinish && form.errors.timeFinish}</p>
                                      {/* fix hiện Lỗi sau khi nhập dữ liệu */}
                                      </div>
                                    </div>
                                  )}
                                />
                        </div>

                      <Button type="primary" onClick={handleSubmit}>Add</Button>
                      <Button type="default" onClick={() => this.close()}>Cancel</Button>
                      </Form>
                )}
              />
          </Modal>
      </div>
    )
  }
}

export default addUserModal
