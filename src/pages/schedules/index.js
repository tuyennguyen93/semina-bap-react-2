import React, { Component } from "react";
import { Table, Button, Popconfirm, Icon, message, Tag} from "antd";
import { connect } from "react-redux";
import "./style.scss";

import AddSchedulesModal from './add-schedules-modal'
import EditSchedulesModal from './edit-schedules-modal'


import { Container, FitHeightContainer, Page } from "app/components";
import { actions } from "app/store/actions";
@connect(state=>({ //lấy dữ liệu  state từ reducers 
  schedulesStore:state.schedules, // lấy cái này : topics: [], //lưu giá trị vào đây
  topicsStore:state.topics
}),{
  getAllSchedules:actions.getAllSchedules, //gọi action từ saga 
  createSchedules:actions.createSchedules,
  updateSchedules:actions.updateSchedules,
  deleteSchedules:actions.deleteSchedules,
  getAllTopics:actions.getAllTopics,
  getAllApproveTopics:actions.getAllApproveTopics

})

export default class Schedules extends Component {

  /**
 * delete
 */

confirm = (e) => {
  const { deleteSchedules } = this.props
  console.log(e.id)
  deleteSchedules(e.id)
  message.success('Delete success!')
  
}

 cancel = (e) => {
  console.log(e)
  message.error('Delete error!')
}

  componentDidMount() {
    this.props.getAllSchedules(); //gọi từ index trong action GET_ALL_TOPICS =>getAllSchedules
    this.props.getAllTopics();
    this.props.getAllApproveTopics()
  }

  render() {
    const { schedules, loading } = this.props.schedulesStore; 
    const {createSchedules,updateSchedules} = this.props
    const {topics, topicsApprove}= this.props.topicsStore
    console.log(schedules);
    

    const columns = [
     
      {
        title: "SPEAKER",
        dataIndex: "speakerName",
        key: "speakerName"
      },
      {
        title: "TOPIC",
        dataIndex: "topic.title",
        key: "topic.title"
      },
      {
        title: "TIME START",
        dataIndex: "timeStart",
        key: "timeStart"
      },
      {
        title: "TIME FINISH",
        dataIndex: "timeFinish",
        key: "timeFinish"
      },
      {
        title: 'ACTION',
        key: 'action',
        render: (row, record) => (
           <span>
             <Button
               type="ghost"
               onClick={() => {
                 this._editSchedulesModal.open(record)
               }}
             ><Icon type="edit" /> 
             </Button>
 
             <Popconfirm
               title="Are you sure delete this schedules?"
               onConfirm={() => this.confirm(record)}
               onCancel={this.cancel}
               okText="Yes"
               cancelText="No"
             >
               <a href="#"><Button type="danger"><Icon type="delete" /></Button></a>
            </Popconfirm>

           </span>
        )
      }
    ];

    return (
      <Page className="home">
        <FitHeightContainer hasHeader>
          <Container>
            <h2>List Schedules</h2>
            <br/>
            <Button
              type="primary"
              onClick={() => {
                this._addSchedulesModal.open()
              }}
            ><Icon type="folder-open" />
              Add Schedule
            </Button> 

            <br/>  <br/>

            <Table
              rowKey={(row, index) => index} //fix lỗi key  bằng cách tạo ra các index ++ ở các dòng
              pagination={true}
              loading={loading}
              dataSource={schedules}
              columns={columns}
             
            />

            <AddSchedulesModal           
              createSchedules={createSchedules}
              topics={topicsApprove}
              ref={(ref) => { this._addSchedulesModal = ref }}
            /> 

            <EditSchedulesModal
              updateSchedules={updateSchedules}
              topics={topicsApprove}
              ref={(ref) => { this._editSchedulesModal = ref }}
            />
          </Container>
        </FitHeightContainer>
      </Page>
    );
  }
}


