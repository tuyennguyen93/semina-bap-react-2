import React from 'react'
import { Modal, Button, Input, Row, Col, Select, Tag, message, DatePicker } from 'antd'
import { Formik, Field, Form } from 'formik'
import moment from 'moment'
import * as yup from 'yup'
import { TYPES } from 'app/store/actions'

const schemaValidation = yup.object().shape({
  topicId: yup.number().required(),
  speakerName: yup.string().required(),
  timeStart: yup.date().required(),
  timeFinish: yup.date().required()
})

class addUserModal extends React.Component {
  state = {
    visible: false,
    user: null
  }

  open = (user) => {
    this.setState({
      visible: true,
      user
    })
  };

  close = (e) => {
    this.setState({
      visible: false,
      user: null
    })
  }

  _onSelectChange = (value) => {
    console.log(value)
  }

  _submit = () => {
    this.close()
    console.log('aaa')
  }

  // event
  _onSubmit = (value) => {
    const dataDetail = {
      id: value.id,
      topic: { id: value.topicId },
      timeStart: value.timeStart.format('YYYY-MM-DD HH:mm'),
      timeFinish: value.timeFinish.format('YYYY-MM-DD HH:mm'),
      speakerName: value.speakerName
    }
    console.log(dataDetail)
    const { updateSchedules } = this.props
    updateSchedules(dataDetail, (action) => {
      if (action === TYPES.UPDATE_SCHEDULES_SUCCESS) {
        this.close()
        message.success('Successfully update schedule!')
      } else {
        this.close()
        message.error('error updated !')
      }
    })
    // this.close()
    // console.log(value)
  }

  render() {
    const { user } = this.state
    const { topics } = this.props
    console.log(' edit sche')
    console.log(user)
    console.log(' edit sche')
    // fix trung data o modal
    const initialValues = user || {
      name: ''
    }
    return (
      <div>
          <Modal
            title="Edit Schedule"
            visible={this.state.visible}
            onCancel={this.close}
            onOk={this._onSubmit}
            footer={null} // ko muốn hiện footer
          >
              <Formik
                enableReinitialize
                initialValues={initialValues}
                validationSchema={schemaValidation}
                onSubmit={this._onSubmit}

                render={({ handleSubmit }) => (
                      <Form>
                            {/* Topics Name */}
                            <Field
                              name="topicId"
                              render={({ field }) => (
                          <div>
                            <p className="label">Topics</p>
                            <Select
                              {...field}
                              onChange={(value) => {
                                field.onChange({ target: { value, name: field.name } })
                              }}
                              onBlur={(value) => {
                                field.onChange({ target: { value, name: field.name } })
                              }}
                            >
                            {topics.map(item => (
                              <Select.Option value={item.id} key={item.id}>{item.title}</Select.Option>
                            ))}

                            </Select>
                          </div>
                           )}
                            />

                        {/* speakerName */}
                        <div className="field">
                                <p className="label">Speaker Name</p>
                                <Field
                                  name="speakerName"
                                  render={({ field, form }) => (
                                    <div>
                                    <Input {...field} />
                                    <p className="error-message">{form.touched.speakerName && form.errors.speakerName}</p>
                                    {/* fix hiện Lỗi sau khi nhập dữ liệu */}
                                    </div>
                                  )}
                                />
                        </div>


                        {/* timeStart */}
                        <div className="field">
                                <p className="label">TimeStart</p>
                                <Field
                                  name="timeStart"
                                  render={({ field, form }) => (
                                    <div>

                                      <div>
                                        <DatePicker
                                          showTime
                                          placeholder="Select Time"
                                          defaultValue={field.value && moment(field.value)}
                                          onOk={value => field.onChange({ target: { value, name: field.name } })}
                                        />
                                      <p className="error-message">{form.touched.timeStart && form.errors.timeStart}</p>
                                      {/* fix hiện Lỗi sau khi nhập dữ liệu */}
                                      </div>
                                    </div>
                                  )}
                                />
                        </div>

                        {/* timeFinish */}
                        <div className="field">
                                <p className="label">TimeFinish</p>
                                <Field
                                  name="timeFinish"
                                  render={({ field, form }) => (
                                    <div>

                                      <div>
                                        <DatePicker
                                          showTime
                                          placeholder="Select Time"
                                          defaultValue={field.value && moment(field.value)}
                                          onOk={value => field.onChange({ target: { value, name: field.name } })}
                                        />
                                      <p className="error-message">{form.touched.timeFinish && form.errors.timeFinish}</p>
                                      {/* fix hiện Lỗi sau khi nhập dữ liệu */}
                                      </div>
                                    </div>
                                  )}
                                />
                        </div>
                      <Button type="primary" onClick={handleSubmit}>Update</Button>
                      <Button type="default" onClick={() => this.close()}>Cancel</Button>
                      </Form>
                )}
              />
          </Modal>
      </div>
    )
  }
}

export default addUserModal
