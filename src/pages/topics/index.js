import React, { Component } from 'react'
import { Table, Radio, Button, Popconfirm, Icon, message, Tag, Tabs } from 'antd'
import { connect } from "react-redux";
import './style.scss'
import { actions,TYPES } from "app/store/actions";
import { Container, FitHeightContainer, Page } from 'app/components'
import AddTopicsModal from './add-topics-modal'
import EditTopicsModal from './edit-topics-modal'
import ViewUserModal from './view-user-modal'
//import Edit

const { TabPane } = Tabs

@connect(state=>({ 
  topicsStore:state.topics,
  categoriesStore:state.categories ,
  profileStore:state.profile
}),{
  getAllTopics:actions.getAllTopics,
  getAllApproveTopics:actions.getAllApproveTopics,
  getAllDenyTopics:actions.getAllDenyTopics,
  getAllMineTopics:actions.getAllMineTopics,
  getAllCategories:actions.getAllCategories,
  createTopics:actions.createTopics,
  updateTopics:actions.updateTopics,
  deleteTopics:actions.deleteTopics,
  getUserById:actions.getUserById
})

export default class Home extends Component {

  /**
   * delete
   */
  confirm = (e) => {
    const { deleteTopics } = this.props
    console.log(e.id)
    deleteTopics(e.id)
    message.success('Delete success!')
    
  }
  
   cancel = (e) => {
    console.log(e)
    message.error('Delete error!')
  }

  getList = (value) => {
    const { getUserById, profileStore } = this.props
    getUserById(value, (action, data) => {
      console.log(data)
      if(action === TYPES.GET_USER_BY_ID_SUCCESS) {   
        this._viewUserModal.open(data)
     }
    })
  }
  state = {
    topics: [],
    topicsApprove:[],
    topicsUnApprove:[],
    mineTopics:[],
    isLoading: false
  }

  async componentDidMount() {
    //this.props.getAllTopics()
    this.props.getAllApproveTopics()
    this.props.getAllDenyTopics()
    this.props.getAllMineTopics()
    this.props.getAllCategories()
  }


  render() {
    const { topicsApprove, topicsUnApprove, mineTopics, isLoading } = this.props.topicsStore
    const {createTopics, updateTopics,getAllDenyTopics, getAllMineTopics, getAllApproveTopics, getUserById }= this.props
    const {categories}= this.props.categoriesStore
    const columnsApprove = [
      {
        title: 'title',
        dataIndex: 'title',
        key: 'title'
      },{
        title: 'description',
        dataIndex: 'description',
        key: 'description',
        render: (row, record) => {
          var description = record.description
          if(description.length>10){
            description = description.substr(0,10)+ "..."  
          }   
         return description
        }
      },{
        title: 'category',
        dataIndex: 'categories',
        key: 'categories',
        render: category1 => (
          <span>
            {
              category1.map(category2 => (<Tag color="red" key={category2.id}>{category2.name}</Tag>))
            }
          </span>
        )
      },{
        title: 'ACTION',
        key: 'action',
        render: (row, record) => (
           <span>

            {/* view user button */}
            <Button 
               id="view"
               type="ghost"
               onClick={() =>
                  this.getList(record.userId)
               }
             ><Icon type="eye" />
             </Button>

           {/* edit button */}
             <Button
               type="ghost"
               onClick={() => {
                 this._editTopicsModal.open(record)
               }}
             ><Icon type="edit" /> 
             </Button>
             
             {/* delete button */}
             <Popconfirm
               title="Are you sure delete this Topics?"
               onConfirm={() => this.confirm(record)}
               onCancel={this.cancel}
               okText="Yes"
               cancelText="No"
             >
               <a href="#"><Button type="danger"><Icon type="delete" /></Button></a>
            </Popconfirm>
           </span>
        )
      }

    ]

    // colume topic unapprove
    const columnsUnApprove = [
      {
        title: 'title',
        dataIndex: 'title',
        key: 'title'
      },{
        title: 'description',
        dataIndex: 'description',
        key: 'description',
        render: (row, record) => {
          var description = record.description
          if(description.length>10){
            description = description.substr(0,10)+ "..."  
          }   
         return description
        }
      },{
        title: 'category',
        dataIndex: 'categories',
        key: 'categories',
        render: category1 => (
          <span>
            {
              category1.map(category2 => (<Tag color="red" key={category2.id}>{category2.name}</Tag>))
            }
          </span>
        )
      },{
        title: 'ACTION',
        key: 'action',
        render: (row, record) => (
           <span>

           {/* edit button */}
             <Button
               type="primary"
               onClick={() => {
                 this._editTopicsModal.open(record)
               }}
             ><Icon type="check-circle" />
             </Button>
             
             {/* delete button */}
             <Popconfirm
               title="Are you sure delete this Topics?"
               onConfirm={() => this.confirm(record)}
               onCancel={this.cancel}
               okText="Yes"
               cancelText="No"
             >
               <a href="#"><Button type="danger"><Icon type="delete" /></Button></a>
            </Popconfirm>

           </span>
        )
      }

    ]

    // colume mine topic 
    const columnsMineTopics = [
      {
        title: 'title',
        dataIndex: 'title',
        key: 'title'
      },{
        title: 'description',
        dataIndex: 'description',
        key: 'description',
        render: (row, record) => {
          var description = record.description
          if(description.length>10){
            description = description.substr(0,10)+ "..."  
          }   
         return description
        }
      },{
        title: 'category',
        dataIndex: 'categories',
        key: 'categories',
        render: category1 => (
          <span>
            {
              category1.map(category2 => (<Tag color="red" key={category2.id}>{category2.name}</Tag>))
            }
          </span>
        )
      }

    ]
    //event click of tab
    function callback(key) {
      console.log(key);
    }
    return (
      <Page className="home">
        <FitHeightContainer hasHeader>
          <Container>
            <h2>Topic List</h2>
            <br/>

            <Button type="primary"
              onClick={() => {
                this._addTopicsModal.open()
              }}
            ><Icon type="file-add" />Add Topic</Button>
              <Tabs defaultActiveKey="1" onChange={callback}>
                {/* Approve Topics */}
                <TabPane tab="Topics Approve" key="1">
                  
                  <Table rowKey={(row, index) => index} pagination={true} loading={isLoading} dataSource={topicsApprove} columns={columnsApprove} />
                </TabPane>

                {/* UnApprove Topics */}
                <TabPane tab="Topics UnApprove" key="2">
                  <Table rowKey={(row, index) => index} pagination={true} loading={isLoading} dataSource={topicsUnApprove} columns={columnsUnApprove} />          
                </TabPane>

                {/* All Topics */}
                <TabPane tab="My Topics" key="3">

                  <Table rowKey={(row, index) => index} pagination={true} loading={isLoading} dataSource={mineTopics} columns={columnsMineTopics} />
                </TabPane>

              </Tabs>

              <AddTopicsModal
                categories = {categories}
                createTopics = {createTopics}
                getAllApproveTopics={getAllApproveTopics}
                getAllMineTopics={getAllMineTopics}
                getAllDenyTopics={getAllDenyTopics}
                ref={(ref) => { this._addTopicsModal = ref }}
            /> 

              <EditTopicsModal
                categories = {categories}
                updateTopics = {updateTopics}
                getAllApproveTopics={getAllApproveTopics}
                getAllMineTopics={getAllMineTopics}
                getAllDenyTopics={getAllDenyTopics}
                ref={(ref) => { this._editTopicsModal = ref }}
              /> 
              <ViewUserModal
                  getUserById={getUserById}
                 ref={(ref) => { this._viewUserModal = ref }}
              />

          </Container>
        </FitHeightContainer>
      </Page>
    )
  }
}
