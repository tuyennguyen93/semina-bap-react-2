import React from 'react'
import { Modal, Button, Input, Row, Col, Select, Tag, message } from 'antd'
import { Formik, Field, Form } from 'formik'
import * as yup from 'yup'
import { TYPES } from 'app/store/actions'
import './style.scss'

const schemaValidation = yup.object().shape({
  title: yup.string(),
  description: yup.string(),
  category: yup.string()
})

class addUserModal extends React.Component {
  state = {
    visible: false,
    user: null
  }

  open = (user) => {
    this.setState({
      visible: true,
      user
    })
  };

  close = (e) => {
    this.setState({
      visible: false,
      user: null
    })
  }

  _onSelectChange = (value) => {
    console.log(value)
  }

  _submit = () => {
    this.close()
    console.log('aaa')
  }

  // event
  _onSubmit = (value) => {
    const { updateTopics, getAllApproveTopics, getAllDenyTopics, getAllMineTopics } = this.props
   
    value = {
      ...value,
      categories: value.categories.map(item => ({
        id: item
      }))
    }
    updateTopics(value, (action) => {
      if (action === TYPES.UPDATE_TOPICS_SUCCESS) {
        getAllApproveTopics()
        getAllDenyTopics()
        getAllMineTopics()
        this.close()
        message.success('updated !')
      } else {
        this.close()
        message.error('cannot updated !')
      }
    })


    this.close()
    // console.log(value)
  }

  render() {
    const { user } = this.state
    const {categories} = this.props
    console.log(user)
    // fix trung data o modal
    const initialValues = {
      ...user,
      categories: user?.categories.map(category => (category.id))
    } || {
      email: '',
      username: '',
      password: '',
      role: '',
      major: '',
      categories: []
    }
    console.log(user)
    return (
      <div>
          <Modal
            title="Edit Topics"
            visible={this.state.visible}
            onCancel={this.close}
            onOk={this._onSubmit}
            footer={null} // ko muốn hiện footer
          >
              <Formik
                enableReinitialize
                initialValues={initialValues}
                validationSchema={schemaValidation}
                onSubmit={this._onSubmit}

                render={({ handleSubmit }) => (
                      <Form>
                      {/* name */}
                      <div className="field">
                              <p className="label">Title</p>
                              <Field
                                name="title"
                                render={({ field, form }) => (
                                  <div>
                                  <Input {...field} />
                                  <p className="error-message">{form.touched.title && form.errors.title}</p>
                                  {/* fix hiện Lỗi sau khi nhập dữ liệu */}
                                  </div>
                                )}
                              />
                      </div>

                        {/* description */}
                        <div className="field">
                                <p className="label">Description</p>
                                <Field
                                  name="description"
                                  render={({ field, form }) => (
                                    <div>
                                    <Input.TextArea
                                      {...field}
                                      placeholder="write description for this topic"
                                      autosize={{ minRows: 2, maxRows: 6 }}
                                    />
                                    <p className="error-message">{form.touched.description && form.errors.description}</p>
                                    </div>
                                  )}
                                />
                         </div>

                       {/* type */}
                       <Field
                         name="type"
                         render={({ field }) => (
                          <div>
                            <p className="label">Type</p>
                            <Select
                              {...field}
                              onChange={(value) => {
                                field.onChange({ target: { value, name: field.name } })
                              }}
                              onBlur={(value) => {
                                field.onChange({ target: { value, name: field.name } })
                              }}
                            >
                              <Select.Option value={0}>Suggest</Select.Option>
                              <Select.Option value={1}>Speaker</Select.Option>
                            </Select>
                          </div>
                         )}
                       />
                       
                        {/* category */}
                        <Field
                          name="categories"
                          render={({ field }) => (
                          <div>
                            <p className="label">category</p>
                            <Select
                              mode="multiple"
                              {...field}
                              onChange={(value) => {
                                field.onChange({ target: { value, name: field.name } })
                              }}
                              onBlur={(value) => {
                                field.onChange({ target: { value, name: field.name } })
                              }}
                            >
                             {categories.map(item => (
                                <Select.Option value={item.id} key={item.id}>
                                <Tag  color="crimson">
                                  {item.name}
                                </Tag>                                   
                                </Select.Option>
                             ))}
                            </Select>
                          </div>
                        )}
                        />
                         {/* status */}
                         <Field
                           name="status"
                           render={({ field }) => (
                          <div>
                            <p className="label">Status</p>
                            <Select
                              {...field}
                              onChange={(value) => {
                                field.onChange({ target: { value, name: field.name } })
                              }}
                              onBlur={(value) => {
                                field.onChange({ target: { value, name: field.name } })
                              }}
                            >
                              <Select.Option value={1}> <Tag color="green">Approve</Tag> </Select.Option>
                              <Select.Option value={0}> <Tag color="volcano">Un Upprove</Tag> </Select.Option>
                            </Select>
                          </div>
                           )}
                         />


                      <Button type="primary" onClick={handleSubmit}>Update</Button>
                      <Button type="default" onClick={() => this.close()} id="btnCancelModal">cancel</Button>
                      </Form>
                )}
              />
          </Modal>
      </div>
    )
  }
}

export default addUserModal
